package com.algoTrader.controllerCluster;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;

import com.algoTrader.brokerCluster.DailyTick;
import com.algoTrader.brokerCluster.PriceTick;
import com.algoTrader.supportCluster.Configuration;

public class StockBean implements Comparable<StockBean> {

	// ----------------------- //
	// - Main bean variables - //
	// ----------------------- //
	
	public String 	ticker;
	public Double 	price;
	public Long 	volume;
	public Calendar lastUpdated;
	public Double 	grade;
	
	// Last price is at the Last Place: (past prices works only for daily trade . sell maximum in the day).
	public LinkedList<Double> 	pastPrices;
	public LinkedList<Calendar>	priceDates;
	public Integer              pricesSamples;
	
	// Candles supporting Logic:
	public LinkedList<Double> 	pastClose;
	public LinkedList<Double> 	pastHigh;
	public LinkedList<Double> 	pastLow;
	public LinkedList<Double> 	pastOpen;
	public LinkedList<Calendar>	dailyDates;
	public Integer				dailySamples;

	// Bean Additional Variables:
	public Boolean 	currentStatus	= false;// indicate stock bought {true, stock had been bought}
	public Double 	buyPrice	= 0.0;
	public Double	buyGrade 	= 0.0;
	public Integer	investedAmount	= 0;

		
	// Indication documentation:
	// -------------------------
	
	public HashMap<String,Boolean> indicatorsTable = new HashMap<String,Boolean>(); 
	
	
	// Configuration:
	// --------------
	
	private Configuration cfg = Configuration.getInstance();
	
	
	// For offlineMode, states the itiration on which the stock had been bought
	public int iterationTrade = 0;
	
	
	// ---------------- //
	// - Constructors - //
	// ---------------- //
	
	// Generic:
	// --------
	
	public StockBean() {
		this.ticker 		= "";
		this.price 			= 0.0;
		this.volume 		= new Long(0);
		this.lastUpdated 	= null;
		this.grade 			= 0.0;
		this.pastPrices 	= null;
		
		this.pastClose 		= null;
		this.pastHigh 		= null;
		this.pastLow 		= null;
		this.pastOpen 		= null;
	}
	
	
	// Short Main variables:
	// ---------------------
	
	public StockBean(String ticker,
			Double 			price,
			long 			volume,
			Calendar 		lastUpdated
			){
		this.ticker			= ticker;
		this.price  		= price;
		this.volume 		= volume;
		this.lastUpdated	= lastUpdated;
		this.grade			= 0.0;
		this.pastPrices  	= new LinkedList<Double>();
		this.priceDates		= new LinkedList<Calendar>();
		this.pricesSamples	= 0;
		
		this.pastClose		= new LinkedList<Double>();
		this.pastHigh		= new LinkedList<Double>();
		this.pastLow 		= new LinkedList<Double>();
		this.pastOpen 		= new LinkedList<Double>();
		this.dailyDates		= new LinkedList<Calendar>();
		this.dailySamples 	= 0;		
	};
	
	
	// Main variables:
	// ---------------
	
	public StockBean(String 			ticker,
					Double 				price,
					long 				volume,
					Calendar 			lastUpdated,					
					LinkedList<Double> 	pastPrices,
					double grade){
		this.ticker			= ticker;
		this.price  		= price;
		this.volume 		= volume;
		this.lastUpdated	= lastUpdated;
		this.pastPrices		= pastPrices;
		this.grade			= grade;
		this.pastPrices 	= pastPrices;
		this.pricesSamples 	= 0;
	};
	
	
	// By Main variables and Past Price Lists (For Candle Stick view)
	// --------------------------------------------------------------
	
	public StockBean(String 			ticker,
					Double 				price,
					Long 				volume,
					Calendar			lastUpdated,					
					LinkedList<Double> 	pastPrices,
					LinkedList<Double> 	pastClose,
					LinkedList<Double> 	pastHigh,
					LinkedList<Double> 	pastLow,
					LinkedList<Double> 	pastOpen,
					Double 				grade){
		this.ticker			= ticker		;
		this.price  		= price			;
		this.volume 		= volume		;
		this.lastUpdated	= lastUpdated	;
		this.pastPrices		= pastPrices	;
		this.grade			= grade			;
		this.pastPrices 	= pastPrices	;
		this.pastClose	 	= pastClose		;
		this.pastHigh	 	= pastHigh		;
		this.pastLow	 	= pastLow		;
		this.pastOpen	 	= pastOpen		;		
	};
	
	
	// Complete Constructor:
	// ---------------------
	
	public StockBean(String 			ticker,
					Double 				grade,		
					Double 				price,
					Long 				volume,	
					Calendar			lastUpdated,
					Boolean				currentStatus,
					Double 				buyPrice,		
					Double 				buyGrade,					
					Integer				investedAmount,
					Integer				pricesSamples,
					Integer				dailySamples,
					LinkedList<Double> 	pastPrices,
					LinkedList<Calendar>priceDates,
					LinkedList<Double> 	pastClose,
					LinkedList<Double> 	pastHigh,
					LinkedList<Double> 	pastLow,
					LinkedList<Double> 	pastOpen,
					LinkedList<Calendar>dailyDates
					){
			this.ticker			= ticker			;
			this.grade			= grade				;	
			this.price			= price			    ;
			this.volume			= volume			;	
			this.lastUpdated	= lastUpdated	    ;
			this.currentStatus	= currentStatus	    ;
			this.buyPrice		= buyPrice			;	
			this.buyGrade		= buyGrade			;	
			this.investedAmount	= investedAmount    ;
			this.pricesSamples	= pricesSamples	    ;
			this.dailySamples	= dailySamples	    ;
			this.pastPrices		= pastPrices		;
			this.priceDates		= priceDates		;
			this.pastClose		= pastClose		    ;
			this.pastHigh		= pastHigh		    ;
			this.pastLow		= pastLow		    ;
			this.pastOpen		= pastOpen		    ;
			this.dailyDates	    = dailyDates	    ;
	}

	
	// ----------------------------- //
	// - Contractor with PriceTick - //
	// ----------------------------- //
	
	
	public StockBean(PriceTick priceTick){

		this.ticker			= priceTick.ticker;
		this.price  		= priceTick.price;
		this.volume 		= priceTick.volume;
		
		this.grade			= 0.0;
		
		this.pastPrices  	= new LinkedList<Double>();		
		this.priceDates		= new LinkedList<Calendar>();
		this.pricesSamples	= 1;
		
		
		this.pastClose		= new LinkedList<Double>();
		this.pastHigh		= new LinkedList<Double>();
		this.pastLow 		= new LinkedList<Double>();
		this.pastOpen 		= new LinkedList<Double>();
		this.dailyDates		= new LinkedList<Calendar>();
		this.dailySamples	= 0;
		
		this.lastUpdated	= priceTick.priceDate;
		
		this.addPrice(this.price);
	}
	
	
	
	// ------------------------------ //
	// - Constructor with DailyTick - //
	// ------------------------------ //
	
	public StockBean(DailyTick dailyTick){

		this.ticker			= dailyTick.ticker;
		this.price  		= dailyTick.price;
		this.volume 		= dailyTick.volume;
		
		this.grade			= 0.0;
		
		this.pastPrices  	= new LinkedList<Double>();
		this.priceDates		= new LinkedList<Calendar>();
		this.pricesSamples	= 1;
		
		this.pastClose		= new LinkedList<Double>();
		this.pastHigh		= new LinkedList<Double>();
		this.pastLow 		= new LinkedList<Double>();
		this.pastOpen 		= new LinkedList<Double>();
		this.dailyDates		= new LinkedList<Calendar>();
		this.dailySamples	= 1;
		
		this.lastUpdated = dailyTick.dailyDate;
		
		this.addPastPrices(dailyTick.price, dailyTick.closePrice, 
				dailyTick.highPrice, dailyTick.lowPrice, dailyTick.openPrice);
	}


	// Copy YapiBean:
	// --------------
	
	public StockBean(StockBean stockBean){
		
		this.ticker					= stockBean.ticker			;
		this.price  				= stockBean.price			;
		this.volume 				= stockBean.volume			;
		this.lastUpdated			= stockBean.lastUpdated		;
		
		this.pastPrices				= stockBean.pastPrices		;
		this.priceDates				= stockBean.priceDates		;
		this.pricesSamples			= stockBean.pricesSamples	;
		
		this.grade					= stockBean.grade			;
		
		this.pastPrices				= stockBean.pastPrices		;
		this.pastClose 				= stockBean.pastClose		;
		this.pastHigh 				= stockBean.pastHigh 		;
		this.pastLow 				= stockBean.pastLow 		;
		this.pastOpen 				= stockBean.pastOpen 		;	
		this.dailyDates				= stockBean.dailyDates		;
		this.dailySamples			= stockBean.dailySamples	;
				
		this.currentStatus			= stockBean.currentStatus	;
		this.buyPrice				= stockBean.buyPrice		;
		this.buyGrade				= stockBean.buyGrade		;
		this.investedAmount			= stockBean.investedAmount	;
		
	}
	
	// TODO: maybe implement addTick for accurate dates
		
	// --------------------------- //
	// - Pricing List convention - //
	// --------------------------- //
	
	// Price & pastPrices:
	// -------------------
	// Bean prices is arranged by order:
	// Price - Current Price of Stock.
	// past Prices : [0] - Current Price
	//				 [1] - Last Tick Price
	//				 [:]
	//				 [:]
	// 				 [n-1] Oldest Price
	
	// ---------------------------------------------------------------------------------- //
	
	// pastDate, pastOpen, pastClose:
	// ------------------------------
	// Bean price is arrange by order,
	// each price is a trade day price, open,close, high & low of the lasr trade day
	// past<####> : [0] - last Day trade.
	// 				[1] - Day  before trade
	//				[:]
	//				[:]
	//				[n-1] - oldest trade day available
	
	// ----------------------------------------------------------------------------------- //
	
	// -------------- //
	// - Add Prices - //
	// -------------- //
		
	public Calendar addPrice (Double price){
		
		// Check maximum amount of sampled reached for price list:
		if (this.pricesSamples >= cfg.maximumNumOfSamples){
			
			// Remove last Price,Date and decrement pricesSample
			this.pastPrices.removeLast();
			this.priceDates.removeLast();
			this.pricesSamples--;
			
		}
		
		this.price = price;
		this.pastPrices.addFirst(price); 
		
		
		Calendar date = new GregorianCalendar();
		this.priceDates.addFirst(date);
		
		this.pricesSamples++;
		
		return date;
	}
	
	public Calendar addPastPrices(	Double		price		,
								Double 		priceClose	,
								Double 		priceHigh	,
								Double 		priceLow	,
								Double 		priceOpen	){
		
		Calendar date = addPrice(price);
		
		// Check maximum amount of samples reached for daily list
		
		if (this.dailySamples >= cfg.maximumNumOfSamples){
			
			// remove last prices from all daily past lists and dates, decrement daily dates
			pastClose.removeLast();
			pastHigh.removeLast();
			pastLow.removeLast();
			pastOpen.removeLast();
			dailyDates.removeLast();			
			this.dailySamples--;			
		}
				
		pastClose.addFirst(priceClose);
		pastHigh.addFirst(priceHigh);
		pastLow.addFirst(priceLow);
		pastOpen.addFirst(priceOpen);
		dailyDates.addFirst(date);
		this.dailySamples++;
		
		return date;
	}
	
	
	
	
	public double[] getListAsArray(LinkedList<Double> list){
		
		double[] outArray = new double[list.size()];
		
		for(int i = 0 ; i < list.size(); i++){
			outArray[i] = (double) list.get(i);			
		}
		
		return outArray;
	}
	
	
	// -------------------------- //
	// - Print for store & load - //
	// -------------------------- //

	public String print(){
		

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy.hh.mm");
		String lastUpdatedString = sdf.format(lastUpdated.getTime());
		
		return ticker +"," + grade + "," + price + "," + volume + "," +  lastUpdatedString + ","  + "," + currentStatus + "," +buyPrice
				+ "," + buyGrade + "," + investedAmount + "," + pricesSamples + "," + dailySamples 
				+ "||" + printLastPrices(pastPrices) + "||" + printLastDates(priceDates);
	}
	
	
	public String printLastPrices(LinkedList<Double> pastPrices){
		String lastPricesString = "";
		
		for (Double nextPrice : pastPrices){
			lastPricesString += nextPrice.toString() + ",";
		}
		
		if (lastPricesString.length() > 0)
			lastPricesString = lastPricesString.substring(0, lastPricesString.length()-1);
		
		return lastPricesString;

	}
	
	public String printLastDates(LinkedList<Calendar> pastDates){
		
		String lastDatesString = Integer.toString(pastDates.size()) + ",";
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy.hh.mm");
		
		for (Calendar nextDate : pastDates){
			lastDatesString += sdf.format(nextDate.getTime()) + ",";
		}
		if (lastDatesString.length() > 0)
			lastDatesString = lastDatesString.substring(0, lastDatesString.length() - 1);
		
		return lastDatesString;
	}
	
	public String printAll(){
		return  print() + "||" + printLastPrices(pastClose) + "||" + printLastPrices(pastHigh) + "||" +printLastPrices(pastLow) + 
				"||" + printLastPrices(pastOpen) + "||" + printLastDates(dailyDates);
	}

	
	// Human readable print:
	// ---------------------
	public String printHuman(){
                String outputString = "\nTicker: " + ticker;
		outputString += "\nGrade: " + grade + ", Current Status: " + currentStatus;
		outputString += "\nbuyPrice: " + buyPrice + ", investedAmount: " + investedAmount + ". iterationTrade: " + iterationTrade;
		outputString += "\n----------------------------------------------";
                	
		return outputString;
	}

	public String printHumanWithPrices(){
                String outputString = printHuman();
                outputString += "\nClose Prices: " + printLastPrices(pastClose);	
                outputString += "\nHigh Prices: " + printLastPrices(pastHigh);	
                outputString += "\nLow Prices: " + printLastPrices(pastLow);	
                outputString += "\nOpen Prices: " + printLastPrices(pastOpen);	
		return outputString;
	}
	
	public int compareTo(StockBean bean) {
		return (this.grade >= bean.grade) ? 
				((this.grade == bean.grade ) ? 0 : 1)  : -1;
	}
	

}
