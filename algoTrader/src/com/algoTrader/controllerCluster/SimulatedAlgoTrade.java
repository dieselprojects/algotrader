package com.algoTrader.controllerCluster;

import java.util.ArrayList;

import com.algoTrader.algoCluster.Algo;
import com.algoTrader.brokerCluster.BrokerTickType;
import com.algoTrader.brokerCluster.DailyTick;
import com.algoTrader.brokerCluster.SimulatedBroker;
import com.algoTrader.brokerCluster.Tick;
import com.algoTrader.supportCluster.AutoConfirm;
import com.algoTrader.supportCluster.Configuration;
import com.algoTrader.supportCluster.FundsMan;
import com.algoTrader.supportCluster.HumanIF;
import com.algoTrader.supportCluster.HumanShellConfirm;
import com.algoTrader.supportCluster.Logger;
import com.algoTrader.supportCluster.TimeMan;
import com.algoTrader.supportCluster.Types.ConfirmationType;
import com.algoTrader.supportCluster.WindowHumanConfirm;
import com.algoTrader.supportCluster.webConfirm;
import com.algoTrader.supportCluster.webConnection;

public class SimulatedAlgoTrade {
	
	// ------------------- //
	// - Class Variables - //
	// ------------------- //
	
	
	// Controller cluster Components:
	// ------------------------------
	
	TradeHolding 		tradeHolding;
	DataBaseController 	databaseControllerHolding;
	
	TradeGeneral 		tradeGeneral;
	DataBaseController 	databaseControllerGeneral;
		
		
	// Algorithm Cluster components:
	// -----------------------------
	
	Algo mainAlgo;
	
		
	// Broker cluster Components:
	// --------------------------
	
	SimulatedBroker Broker;
	
	
	// Support cluster components:
	// ---------------------------
	
	FundsMan 		funds;
	HumanIF 		humanConfirm;
	TimeMan			timeMan;
	Configuration	configuration;
	Logger			logger;
	webConnection	webConnection;
	
	
	// --------------- //
	// - Constructor - //
	// --------------- //
	
	public SimulatedAlgoTrade(){
		
		// Support modules:
		// ----------------
		
		System.out.println("__Constructor__ Simulated AlgoTrade");
		
		System.out.println("Creating Configuration Tree");
		configuration = Configuration.getInstance();

	
		switch (configuration.confirmationType){
		case AUTO_CONFIRMATION:
			System.out.println("Creating Human Inteface Module, Auto confirmation");
			humanConfirm = new AutoConfirm();
			break;
		case SHELL_CONFIRMATION:
			System.out.println("Creating Human Inteface Module, Shell confirmation");
			humanConfirm = new HumanShellConfirm();
			break;
		case GUI_CONFIRMATION:
			System.out.println("Creating Human Interface module, GUI confirmation");
			humanConfirm = new WindowHumanConfirm();
			break;
		case WEB_CONFIRMATION:
			System.out.println("Creating Human Interface module, Web Confirmation");
			humanConfirm = new webConfirm();
			webConnection = new webConnection();
			break;
		default:
        	System.err.println("Not valid confirmationType selection");
        	System.err.println("Exiting...");
        	System.exit(1);
			break;
		}
		
		System.out.println("Creating Time Manager Module");
		timeMan = new TimeMan();
		
				
		
		
		// Broker Cluster:
		// ---------------
		System.out.println("Creating Simulated Broker Cluster");
		Broker = SimulatedBroker.getInstance();
		
		
		System.out.println("Creating Logger Module");
		logger = Logger.getInstance();
		
		System.out.println("Creating Funds Manager Module with Initial funds of " + configuration.initialFunds);
		funds = FundsMan.getInstance();   	
		
		// Controller Cluster:
		// -------------------

		
		System.out.println("Creating DataBase Controller for Holding Stocks");
		databaseControllerHolding = new DataBaseController("HoldingDBController");
		
		System.out.println("Creating DataBase Controller for General Stocks");
		databaseControllerGeneral =  new DataBaseController("GeneralDBController");
				
		System.out.println("Creating Trade Holding - for Selling Stocks");
		tradeHolding = new TradeHolding(databaseControllerHolding,Broker, humanConfirm, funds, logger);
		
		System.out.println("Creating Trade General - for buying Stocks");
		tradeGeneral = new TradeGeneral(databaseControllerGeneral,Broker, humanConfirm, funds, logger);	
		
		
		System.out.println("Finished Simulated AlgoTrade Constructor");
	}
	
	
	public void SimulationStarter(){
		
		System.out.println("___Started Simulation___");
		
		System.out.println("*********************************************");
		System.out.println("**    Simulated ALGOTRADER - SETUP PHASE   **");
		System.out.println("*********************************************");

		Setup();
		
	
		System.out.println("*******************************************");
		System.out.println("**  Simulated ALGOTRADER - TRADE PHASE   **");
		System.out.println("*******************************************");
		
		Trade();
		
		System.out.println("********************************************");
		System.out.println("**   Simulated ALGOTRADER - FINAL PHASE   **");
		System.out.println("********************************************");
		
		Final();
		
	}
	
	
	// --------- //
	// - SETUP - //
	// --------- //
	
	private void Setup(){
		// Add StocksBean to DataBase and add prices, 
		
		// Simulated Algo Setup phase:
		// ---------------------------
		
		// At this point there's full broker database ready.
		// a. Call Broker and get all stored Tickers.?? not sure if it's should be in the broker class
		// b. use tickers to Call Broker and get DailyTicks
		// c. AdvIteration
		// d. call sub method Convert Tick to StockBean
		// e. Use ConvertMethod to create ArrayList of StockBean
		// f. Use ArrayList of StockBeans to push into the DataBase
		// g. use addStockBean to #setupIteration to GeneralDataBase.
		
		
		// ------------------------------------- //
		// -- Specific Additional Setup Tasks -- //
		// ------------------------------------- //
		
		// Add WebConnection Interface:
		// ----------------------------
		if (configuration.confirmationType == ConfirmationType.WEB_CONFIRMATION){
			webConnection.sendMail();
			webConnection.createMySQL();
		}
		
		// -------------------------------- //
		// -- End Additional Setup Tasks -- //
		// -------------------------------- //
		
		// a. Call Broker and get all stored Tickers.?? not sure if it's should be in the broker class
		ArrayList<String> allTickerArr = Broker.getAllTickers(false);

		// b. use tickers to Call Broker and get DailyTicks
		ArrayList<Tick>  firstTickArr = Broker.getTick(allTickerArr, BrokerTickType.DailyTick); 
		
		// c. AdvIteration
		Broker.advIteration();
		
		// d. call sub method Convert Tick to StockBean
		// e. Use ConvertMethod to create ArrayList of StockBean
		
		ArrayList<StockBean> newStockBeanArr = convertDailyTickToBean(firstTickArr);
		
		// f. Use ArrayList of StockBeans to push into the DataBase

		databaseControllerGeneral.addStockBean(newStockBeanArr);
		
		
		// g. use addStockBean to add all.
		
		for (int i = 1 ; i < Configuration.getInstance().setupTicks ; i++){
			
			ArrayList<Tick> dailyTickArr = Broker.getTick(Broker.getAllTickers(false), BrokerTickType.DailyTick);
			databaseControllerGeneral.updateBeanPrices(dailyTickArr, BrokerTickType.DailyTick);
			
			Broker.advIteration();
			
			
		}
		
	}

	// ----------------- //
	// -  Trade phase  - //
	// ----------------- //
	
	private void Trade(){
		
		while ( Broker.advIteration() ){
			
			timeMan.waitUntilNextTick();			
			System.out.println("--------------------------------------");
			System.out.println("--    IN TRADE PHASE, ITERATION "+Broker.getIteration() +"  --");
			System.out.println("--------------------------------------");			
			

			System.out.println("Starting Trade Holding:");
			System.out.println("-----------------------");
			ArrayList<StockBean> selledstocks = tradeHolding.startIteration(BrokerTickType.DailyTick);

			System.out.println("Starting Trade General:");
			System.out.println("-----------------------");
			ArrayList<StockBean> boughtStocks = tradeGeneral.startIteration(BrokerTickType.DailyTick);
			
			// TODO, planning problem trade's should not be treated as DataBase controllers
			// tradeHolding/generalHolding
			// Also, add StopLost to algorithm
			// Add minimum iteration before sell bought stock.
			
			databaseControllerHolding.addStockBean(boughtStocks);
			databaseControllerGeneral.addStockBean(selledstocks);			
			
		}		
	}
	
	// --------- //
	// - FINAL - //
	// --------- //
	
	private void Final(){
		
		// a. Sell all holding Stocks for Current Price
		// b. report
		
		tradeHolding.sellAll();
		
		// for web confirmation, destroy SQL database		
		if (configuration.confirmationType == ConfirmationType.WEB_CONFIRMATION){
			webConnection.destroyMySQL();
		}
			
		System.out.println(logger.printAll());
	}
	
	
	// Convert Tick to stockBean:
	// --------------------------
	
	private StockBean convertDailyTickToBean(Tick tick){
		DailyTick dailyTick = (DailyTick) tick;
		StockBean bean = new StockBean(dailyTick);
		return bean;		
	}
	
	
	private ArrayList<StockBean> convertDailyTickToBean(ArrayList<Tick> tickArr){
		
		ArrayList<StockBean> beanArr = new ArrayList<StockBean>(tickArr.size());
		
		for (Tick nextTick : tickArr){
			beanArr.add(convertDailyTickToBean(nextTick));			
		}
		
		return beanArr;		
	}
	
	
	
	


}
