package com.algoTrader.controllerCluster;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import com.algoTrader.brokerCluster.BrokerTickType;
import com.algoTrader.brokerCluster.DailyTick;
import com.algoTrader.brokerCluster.PriceTick;
import com.algoTrader.brokerCluster.Tick;
import com.algoTrader.supportCluster.Logger;

public class DataBaseController {
	
	// ------------------- //
	// - Class Variables - //
	// ------------------- //
		
	StockDB dataBase;
	Logger logger = Logger.getInstance();
	
	
	// --------------- //
	// - Constructor - //
	// --------------- //
	
	DataBaseController(String name){
		
		dataBase = new StockDB(name + "_InternalDB");		
	}

	
	// ----------------- //
	// - Class methods - //
	// ----------------- //
		
	
	// ----------------------------------- //
	// - Get StockBean & StockBean Array - //
	// ----------------------------------- //
	
	public StockBean getStockBean(String ticker){
		return dataBase.getBean(ticker);
	}
	
	
	public ArrayList<StockBean> getStockBean(ArrayList<String> stockBeanTickerArr){
		
		ArrayList<StockBean>  stockBeanArray = new ArrayList<StockBean>(stockBeanTickerArr.size());
		
		for (String nextTicker : stockBeanTickerArr){
			if (dataBase.isExist(nextTicker)){
				stockBeanArray.add(dataBase.getBean(nextTicker));
			} else {
				System.err.println("Ticker ERROR in DataBase: " + dataBase.dataBaseName +"- no ticker Value for " + nextTicker );
			}
		}
		
		return stockBeanArray;
		
	}
	
	// Get all Stocks by Bean:
	// -----------------------
	
	public ArrayList<StockBean> getAllStockBean(){
		
		LinkedList<StockBean> stockList = dataBase.getAllBeans();
		ArrayList<StockBean> stockArray = new ArrayList<StockBean>(stockList.size());
		
		for (StockBean nextStock : stockList){
			stockArray.add(nextStock);
		}
		
		return stockArray;
		
	}
	
	// Get all Stocks by string:
	// -------------------------
	
	public ArrayList<String> getAllStocksBean(){
		
		ArrayList<StockBean> stockArray = getAllStockBean();
		ArrayList<String> stockStringList = new ArrayList<String>(stockArray.size());
		
		for (StockBean nextStock : stockArray){
			stockStringList.add(nextStock.ticker);
		}
		
		return stockStringList;
		
	}
	
	
	// ----------------------------------- //
	// - Add StockBean & StockBean Array - //
	// ----------------------------------- //
	
	public void addStockBean(StockBean bean){
		dataBase.addBean(bean);
	}
	
	public void addStockBean(ArrayList<StockBean> stockBeanArr){
		
		for (StockBean nextBean : stockBeanArr){
			addStockBean(nextBean);
		}
	}
	
	// -------------------------------------------- //
	// - add Stockbean & StockBean Array by Ticks - //
	// -------------------------------------------- //
	
	public void addStockBean(Tick tick,BrokerTickType brokerTickType){
		
		StockBean bean;
		if (brokerTickType == BrokerTickType.PriceTick){
			bean = new StockBean((PriceTick) tick);
		} else {
			bean = new StockBean((DailyTick) tick);
		}
		
		dataBase.addBean(bean);		
	}
	
	public void addStockBean(ArrayList<Tick> ticksArr,BrokerTickType brokerTickType){
		
		for (Tick nextTick : ticksArr){
			addStockBean(nextTick, brokerTickType);
		}	
	}
	
	
	// ------------------------------------ //
	// -         updateBeanPrices         - //
	// - Update Data base with new Prices - //
	// ------------------------------------ //
	
	public Boolean updateBeanPrices(Tick nextTick, BrokerTickType tickType){
		
		// Get tick.ticker StockBean, 
		// Update Prices in regard to tickType
		// write back.
		// if ticker doesn't exist, return false, and ERR
		
		if ( dataBase.isExist(nextTick.ticker) == false ){
			System.err.println("__ERR__, updateBeanPrices for: " + dataBase.dataBaseName + ", Failed, ticker: "+ nextTick.ticker + ", not exists");
			return false;
		}
		
		// Get bean
		StockBean bean = getStockBean(nextTick.ticker);
			
		if (tickType == BrokerTickType.DailyTick){
			
			// Daily Tick:
			// -----------
			

			// Get Daily Ticker from Tick:
			DailyTick dailyTick = (DailyTick) nextTick;
			if (dailyTick.highPrice == 0 && dailyTick.lowPrice == 0 ){
				System.err.println("__ERR__, casting to daily Tick from tick failed");
				System.exit(1);
			}
			
			// Update Prices
			bean.addPastPrices(dailyTick.price, dailyTick.closePrice, dailyTick.highPrice, dailyTick.lowPrice, dailyTick.openPrice);
			
			
		} else {
			
			// Price Tick:
			// -----------
			
			PriceTick priceTick = (PriceTick) nextTick;
			
			bean.addPrice(priceTick.price);
			
			
		}
		
		// WriteBack:
		dataBase.addBean(bean);
		
		return true;		
		
	}
	
	
	// ------------------------------------------ //
	// -          updateBeansPrice              - //
	// - Update DataBase with new Prices Vector - //
	// ------------------------------------------ //
	
	public ArrayList<Boolean> updateBeanPrices(ArrayList<Tick> nextTickArr, BrokerTickType tickType){
		
		ArrayList<Boolean> returnList = new ArrayList<Boolean>(nextTickArr.size());
		
		
		for (Tick nextTick : nextTickArr){
			returnList.add(updateBeanPrices(nextTick,tickType));
		}
		
		return returnList;
	
	}
	
	
	
	// -------------------------------------- //
	// - Remove StockBean & Stockbean Array - //
	// -------------------------------------- //
	
	public StockBean removeStockBean(StockBean bean){
		return dataBase.removeBean(bean);		
	}
	
	public ArrayList<StockBean> removeStockBean(ArrayList<StockBean> stockBeanArr){
		
		ArrayList<StockBean>  stockBeanReturned = new ArrayList<StockBean>(stockBeanArr.size());
		
		for (StockBean nextBean: stockBeanArr){
			if (dataBase.isExist(nextBean)){
				stockBeanReturned.add(removeStockBean(nextBean));
			} else {
				System.err.println("Ticker Removal ERROR in DataBase: " + dataBase.dataBaseName +"- no ticker Value for " +  nextBean.ticker);
			}
		}
		
		return stockBeanReturned;
	
	}
	
	public Integer dataBaseSize(){
		return dataBase.size;
	}
	
	
	// --------- //
	// - PRINT - //
	// --------- //	
	
	public String print(){	
		return dataBase.printDataBase();
	}
	
	
	// --------------------- //
	// - STORE to HardDisk - //
	// --------------------- //
	
	public void storeDataBase(String file) {
		
		try {
			PrintWriter writer = new PrintWriter(file, "UTF-8");
			writer.print(dataBase.storeDataBase());
			writer.close(); 			
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			System.err.println("Failed to store DataBase: " + dataBase.dataBaseName);
			e.printStackTrace();
			System.exit(2);
		}
		logger.addEvent("Finished storeing dataBase" + dataBase.dataBaseName);
	}
	
	
	// ------------------ //
	// - Load Data Base - //
	// ------------------ //
	
	public void loadDataBase(String file) throws NumberFormatException, IOException{
		
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;

		// first line is General DataBase parameters:
		line = br.readLine();
		String[] generalDBInfo = line.split(",");
		
		// get general database information:
		String dataBaseName 	= generalDBInfo[0];
		Integer stockSamples	= Integer.valueOf(generalDBInfo[1]);
		Integer dataBaseSize	= Integer.valueOf(generalDBInfo[2]);
		
		// Create new dataBase here
		StockDB newDB = new StockDB(dataBaseName, stockSamples, dataBaseSize);
		
        while ((line = br.readLine()) != null) {
        	StockBean nextBean;
        	
        	//Split stock information and past prices 
        	String[] stock = line.split("||");
        	
        	// Get stock information:
        	String[] stockInfo = stock[0].split(",");
        	
        	String 	stockTicker 	= stockInfo[0];
        	Double 	stockGrade	= Double.valueOf(stockInfo[1]);
        	Double 	stockPrice	= Double.valueOf(stockInfo[2]);
        	Long 	stockVolume 	= Long.valueOf(stockInfo[3]);
        	
        	Calendar stockLastUpdated = new GregorianCalendar();
        	try {
            	
        		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy.hh.mm");
        		stockLastUpdated.setTime(sdf.parse(stockInfo[4]));
        	
        	} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
        	
        	Boolean stockCurrentStatus 	= Boolean.valueOf(stockInfo[5]);
        	Double 	stockBuyPrice 		= Double.valueOf(stockInfo[6]);
        	Double 	stockBuyGrade 		= Double.valueOf(stockInfo[7]);
        	Integer stockInvestedAmount = Integer.valueOf(stockInfo[8]);
        	Integer stockPriceSamples 	= Integer.valueOf(stockInfo[9]);
        	Integer stockDailySamples 	= Integer.valueOf(stockInfo[10]);
        	
        	// Get stock past prices:  	
        	LinkedList<Double> stockPastPrices 	= loadPriceList(stock[1]);
        	LinkedList<Double> stockPastClose 	= loadPriceList(stock[3]);
        	LinkedList<Double> stockPastHigh 	= loadPriceList(stock[4]);
        	LinkedList<Double> stockPastLow 	= loadPriceList(stock[5]);
        	LinkedList<Double> stockPastOpen 	= loadPriceList(stock[6]);
        	
        	// Get stock past dates:
        	LinkedList<Calendar> stockPricesDates 	= loadDateList(stock[2]);
        	LinkedList<Calendar> stockDailyDates	= loadDateList(stock[7]);
        	
        	// Create new bean:
        	
        	nextBean = new StockBean(
        			        stockTicker,
        			        stockGrade,	
        			        stockPrice,	
        			        stockVolume,
        			        stockLastUpdated,
        			        stockCurrentStatus, 	
        			        stockBuyPrice, 		
        			        stockBuyGrade, 		
        			        stockInvestedAmount, 
        			        stockPriceSamples, 	
        			        stockDailySamples, 
        			        stockPastPrices,
        			        stockPricesDates, 
        			        stockPastClose, 	
        			        stockPastHigh, 	
        			        stockPastLow, 	
        			        stockPastOpen,
        			        stockDailyDates
       			);
        	

        	dataBase.addBean(nextBean);
        }
        br.close();
        
        // set newDB as database:
        this.dataBase = newDB;
        logger.addEvent("finish loading dataBase" + dataBase.dataBaseName);
        
	}
	
	private LinkedList<Double> loadPriceList(String priceString){
		
		LinkedList<Double> returnedList = new LinkedList<Double>();
		
		String[] priceArr = priceString.split(",");
		
		for (String nextPrice : priceArr){
			returnedList.addLast(Double.valueOf(nextPrice));
		}
		
		return returnedList;
	}
	
	private LinkedList<Calendar> loadDateList(String dateString){
		
		LinkedList<Calendar> returnedList = new LinkedList<Calendar>();
		
		String[] dateArr = dateString.split(",");
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy.hh.mm");
		
		for(String nextDate : dateArr){
        	try {
        		Calendar newDate = new GregorianCalendar(); 
        		newDate.setTime(sdf.parse(nextDate));
        		returnedList.addLast(newDate);
        	} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
        	}
		}
		
		
		return returnedList;
		
	}
	

}
