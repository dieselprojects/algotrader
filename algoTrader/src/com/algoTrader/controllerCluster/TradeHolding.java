package com.algoTrader.controllerCluster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import com.algoTrader.algoCluster.AllAlgo;
import com.algoTrader.brokerCluster.Bid;
import com.algoTrader.brokerCluster.BrokerBidType;
import com.algoTrader.brokerCluster.BrokerIF;
import com.algoTrader.brokerCluster.SimulatedBroker;
import com.algoTrader.supportCluster.FundsMan;
import com.algoTrader.supportCluster.HumanIF;
import com.algoTrader.supportCluster.Logger;
import com.algoTrader.supportCluster.Types.OPType;

// This class can only sell Beans


public class TradeHolding extends Trade{

	
	public TradeHolding(DataBaseController 	databaseController,
						BrokerIF 	broker,
						HumanIF 	humanIF, 
						FundsMan 	funds, 
						Logger 		logger) {
		
		super(databaseController,broker, humanIF, funds, logger);
		System.out.println("__Constructor__ TradeHolding");
		
		databaseController = new DataBaseController("tradeHolding Controller");
		//algo = new AllAlgo(OPType.Sell);
		
	}
	
	
	// UseCase Sell:
	// -------------
	//
	// 1. Call TradeHolding.startIteration() - start trading this iteration
	// 2. preTrade - Update DataBase (HoldingDB - with information from SimulatedBroker) Input (Type - Sell)
	// 3. Grade - Grade all stocks in the DataBase and return a Sorted Linkedlist of StockGrade with Grades - GradedList
	// 4. Trade - Input GradedList, Phases:
	//		a. Create Bids LinkedList, each Bid contain price, amount to sell, type and the Stock
	//		b. HumanIf gets the Bids Vector, request approval/edits and return LinkedList of approvedBids (same as bids)
	//		c. broker gets approvedBids and place Bids online- Can take time. - returns DoneBids vector. (some can not be done)
	//		d. funds get the DoneBids and removeFunds according to the doneBids
	//		e. funds use DoneBid to update the doneBid.Stock
	//		f. funds Logs events and returns the doneBids.
	//		g. Under Trade, Stock is taken from doneBids and placed in ArrayList<Stocks> tradedStocksList -> Output value
	// 5. PostTrade gets tradedStocksList and remove that from DataBase
	// 6. TradeHolding.startIteration() returns the tradedStockList to be placed in the otherDataBase.
	

	
	// --------- //
	// - Grade - //
	// --------- //
	
	// General:
	// 3. Grade - Grade all stocks in the DataBase and return a Sorted Linkedlist of StockGrade with Grades - GradedList
	// Grade.
	// Input:
	// output: Sorted LinkedList
	
	public LinkedList<StockBean> Grade() {
		
		LinkedList<StockBean> sortedGradeStocks = new LinkedList<StockBean>(); 
		Double minimumGrade = configuration.minGradeSell;
		
		// Get all stock from database controller:
		ArrayList<StockBean> allStocks = databaseController.getAllStockBean();
		
		for (StockBean nextStock : allStocks){
			
			// creating thread for each stock and grade all stocks concurrently 
			algo = new AllAlgo(nextStock,OPType.SELL);
			algo.run();
			nextStock.grade = algo.getGrade();
			logger.addEvent(algo.printAlgoResults());
			
			// TODO: change implementation, remove casting
			// this logic should determine if the stock had been bought recently, if so don't sell
			if ( ( ((SimulatedBroker) broker).getIteration() - nextStock.iterationTrade ) > 10 ){
				if (nextStock.grade >= minimumGrade){
					sortedGradeStocks.add(nextStock);
				}
			}
		}
		
		Collections.sort(sortedGradeStocks);		
		
		return sortedGradeStocks;
	}

	
	// --------- //
	// - TRADE - //
	// --------- //
	// 4. Trade - Input GradedList, Phases:
	//		a. Create Bids LinkedList, each Bid contain price, amount to sell, type and the Stock
	//		b. HumanIf gets the Bids Vector, request approval/edits and return LinkedList of approvedBids (same as bids)
	//		c. broker gets approvedBids and place Bids online- Can take time. - returns DoneBids vector. (some can not be done)
	//		d. funds get the DoneBids and removeFunds according to the doneBids
	//		e. funds use DoneBid to update the doneBid.Stock
	//		f. funds Logs events and returns the doneBids.
	//		g. Under Trade, Stock is taken from doneBids and placed in ArrayList<Stocks> tradedStocksList -> Output value	
	
	public ArrayList<StockBean> trade(LinkedList<StockBean> sortedGradeStocks) {
		

		// a. Create Bids LinkedList, each Bid contain price, amount to sell, type and the Stock	
		LinkedList<Bid> BidsList = new LinkedList<Bid>();
		
		for (StockBean nextSellStock : sortedGradeStocks){
			
			// Currently sell all
			// TODO: add strategy Support module
			int amountToSell = nextSellStock.investedAmount;
			
			Bid nextBid = new Bid(nextSellStock.ticker	, 
					nextSellStock.price 				,
					amountToSell 						,	 
					BrokerBidType.Sell					,
					nextSellStock);
			
			BidsList.addLast(nextBid);			
			
		}
		
		// b. HumanIf gets the Bids Vector, request approval/edits and return LinkedList of approvedBids (same as bids):
		
		HumanIF humanConfirm = /*(HumanShellConfirm)*/  humanIF;
		LinkedList<Bid> approvedBids = humanConfirm.getConfirmation(BidsList);
	
		
		// c. broker gets approvedBids and place Bids online- Can take time. - returns DoneBids vector. (some can not be done)
		
		LinkedList<Bid> doneBids = broker.placeBids(approvedBids);
		
		// g. Under Trade, Stock is taken from doneBids and placed in ArrayList<Stocks> tradedStocksList -> Output value
		
		ArrayList<StockBean> tradedStockList = new ArrayList<StockBean>(doneBids.size());
		
		for (Bid nextBid : doneBids){
			tradedStockList.add(nextBid.stock);
		}
		
		return tradedStockList;
	}
	
	
	// --------------------------------- //
	// - Special Trade Holding Methods - //
	// --------------------------------- //
	
	// ------------ //
	// - SELL ALL - //
	// ------------ //
	
	// General: for simulation endOftest or for market crush,
	// Works as startIteration
	
	public ArrayList<StockBean> sellAll(){
		
		// a. PreTrade - for stock pricing update:
		// b. NoGrade needed - Selling all, Generate ListOf Bids
		// c. Trade Bids
		
		
		//preTrade(BrokerTickType.DailyTick);
		
		configuration.minGradeSell = 0.0;
		
		LinkedList<StockBean> sortedGradeStocks = Grade();
		
		ArrayList<StockBean> tradedStockList = trade(sortedGradeStocks);
		
		postTrade(tradedStockList);
		
		return tradedStockList;
		
	}
	
	
	
	
	

}
