package com.algoTrader.algoCluster;

//import com.algoTrader.controllerCluster.OPType;
import com.algoTrader.controllerCluster.StockBean;
import com.algoTrader.supportCluster.Configuration;
import com.algoTrader.supportCluster.Logger;
import com.algoTrader.supportCluster.Types.OPType;


// execute all algorithms and indicators and return total grade for each stock.
// this class implements runnable to support concurrency grading for multiple 
// stocks

public class AllAlgo extends Algo implements Runnable{

	Configuration cfg = Configuration.getInstance();
	Logger logger = Logger.getInstance();
	
	
	StockBean  	stock;
	OPType 		operationType;
	Double		stockGrade;	
	
	
	
	// ---------------------------- //
	// -- Algo-trader indicators -- //
	// ---------------------------- //
	// TODO: 
	// Add algorithems/Indicators here:	
	MACDAlgo 	macdAlgo;
	SRSIAlgo 	srsiAlgo;
	Indicators	indicatorsAlgo;


	// Grades for loggers:
	private Double macdGrade;
	private Double srsiGrade;
	private Double indicatorsGrade;
	
	public AllAlgo(StockBean stock,
			OPType operationType){
	
		this.stock = stock;
		
		if ( operationType == OPType.BUY ){
			
			macdAlgo 		= new MACDAlgo();
			srsiAlgo 		= new SRSIAlgo();
			indicatorsAlgo 	= new Indicators();
			
		} else if ( operationType == OPType.SELL ){
			
			macdAlgo = new MACDAlgo();
			srsiAlgo = new SRSIAlgo();
			indicatorsAlgo = new Indicators();
			
		}
	}
	
	
	protected double start(StockBean stock,
			OPType operationType) {
		
		this.macdGrade		 = macdAlgo.start(stock, operationType);
		this.srsiGrade		 = srsiAlgo.start(stock, operationType);
		
		// check minimum daily ticks for indicator base grading:
		if (stock.dailySamples >= cfg.minDailySamples)
			this.indicatorsGrade = indicatorsAlgo.start(stock, operationType);
		else
			logger.addEvent("__ALGO__ not enought daily samples to trade " + stock.ticker);
		
		//TODO: all grading policy here!!!
	
		this.stockGrade = this.macdGrade + this.srsiGrade + this.indicatorsGrade;
		return this.stockGrade;
		
	}

	@Override
	public void run() {
		this.start(this.stock,this.operationType);
	}
	
	public Double getGrade(){
		return this.stockGrade;
	}
	
	public String printAlgoResults(){
		String results = "";
		results += "---------------------------\n";
		results += "Ticker: " + stock.ticker + " Grade: " + this.stockGrade + "\n";
		results += "macdGrade: " + this.macdGrade + " srsiGrade: " + this.srsiGrade + " indicatorsGrade: " + this.indicatorsGrade + "\n";
		return results;
	}

}
