package com.algoTrader.algoCluster;

import com.algoTrader.controllerCluster.StockBean;
import com.algoTrader.supportCluster.MACDCfg;
import com.algoTrader.supportCluster.Types.OPType;
import com.tictactec.ta.lib.*;

public class MACDAlgo extends Algo{
	
	public double start(StockBean stock,OPType operationType){
		
		// Variables:
		// ----------
		double[] tickPrice = new double[stock.pastPrices.size()];
		
		RetCode macdRcore;
		Core macd = new Core();
		
		MInteger outBegIdx		= new MInteger();
	    MInteger outNBElement	= new MInteger();
	    double[] outMACD		= new double[stock.pastPrices.size()];
	    double[] outMACDSignal 	= new double[stock.pastPrices.size()];
	    double[] outMACDHist	= new double[stock.pastPrices.size()];
		
		
		MACDCfg.getInstance();
		int startIndex = tickPrice.length - MACDCfg.macdSimTicksParam;
		int endIndex = tickPrice.length - 1;
		
		// Algorithm:
		// ----------
		
		// Getting all tick prices
		for(int i = stock.pastPrices.size() - 1 ; i >= 0 ; i--){
			tickPrice[i] = (double) stock.pastPrices.get(i);
		}
		
		
		
				
		// Declaration of macdExt:
		// -----------------------
		/*
		  public RetCode macdExt( int startIdx,
			      int endIdx,
			      double inReal[],
			      int optInFastPeriod,
			      MAType optInFastMAType,
			      int optInSlowPeriod,
			      MAType optInSlowMAType,
			      int optInSignalPeriod,
			      MAType optInSignalMAType,
			      MInteger outBegIdx,
			      MInteger outNBElement,
			      double outMACD[],
			      double outMACDSignal[],
			      double outMACDHist[] )
		*/
		
		
		macdRcore = macd.macdExt(startIndex, endIndex, tickPrice, 
				MACDCfg.macdFastParam, MACDCfg.macdMATypeFastParam, 
				MACDCfg.macdSlowParam, MACDCfg.macdMATypeSlowParam, 
				MACDCfg.macdSignalParam, MACDCfg.macdMATypeSignalParam, 
				outBegIdx, outNBElement, outMACD, outMACDSignal, outMACDHist);
		
		if ( macdRcore == RetCode.Success){		
			double result = getGrade(outMACDHist,operationType);
			
			if (result > 0){
				stock.indicatorsTable.put("MACD", true);
				
			} else {
				stock.indicatorsTable.put("MACD", false);
			}
			
			
			return result;
		
		
		} else {
			// Algorithm failed:
			// ----------------
			System.err.println("MACD algorithm failed for Stock - " + stock.ticker);
			return 0;
		}
	
	}
	
	private double getGrade(double[] macdHist,OPType operationType){
		
		int result = 0;
		double lastTickPrice = macdHist[0];
		
		if (operationType == OPType.BUY){
			
			// buy:
			// ----
			// 1. if 'macdTickBeforeParam previous is smaller than 0 (optional - add parameter or change logic for slope)
			// 2. if current tick price is bigger than -'macdIndicaitonMargain (optional - add different parameters for buy/sell)
			
			if (lastTickPrice > -MACDCfg.macdIndicaitonMargain ){
				
				result = 1;
				for (int i = 0  ; i < MACDCfg.macdTickBeforeParam  ; i++ ){
					if (macdHist[i] > 0){
						result = 0;
					}
				}
			}			
		
		} else {
			
			// Sell:
			// -----
			// 1. if 'macdTickBeforeParam previous is bigger than 0 (optional - add parameter or change logic for slope)
			// 2. if current tick price is smaller than 'macdIndicaitonMargain
			if (lastTickPrice < MACDCfg.macdIndicaitonMargain ){
				
				result = 1;
				for (int i = 0  ; i < MACDCfg.macdTickBeforeParam ; i++ ){
					if (macdHist[i] < 0){
						result = 0;
					}
				}
			}
			
		}
	
		return result;
	}
	
	
}
