package com.algoTrader.algoCluster;

import com.algoTrader.controllerCluster.StockBean;
import com.algoTrader.supportCluster.SRSICfg;
import com.algoTrader.supportCluster.Types.OPType;
import com.tictactec.ta.lib.*;

public class SRSIAlgo extends Algo{
	
	public double start(StockBean stock,OPType operationType){
		
		// Variables:
		// ----------
		double[] tickPrice = new double[stock.pastPrices.size()];
		
		RetCode srsiRcore;
		Core srsi = new Core();
		
		MInteger outBegIdx		= new MInteger();
	    MInteger outNBElement	= new MInteger();
	    double[] outSRSIK		= new double[stock.pastPrices.size()];
	    double[] outSRSID 		= new double[stock.pastPrices.size()];
	    		
		
		SRSICfg.getInstance();
		int startIndex = tickPrice.length - SRSICfg.srsiSimTicksParam;
		int endIndex = tickPrice.length - 1;
		
		// Algorithm:
		// ----------
		
		// Getting all tick prices
		for(int i = stock.pastPrices.size() - 1 ; i >= 0  ; i--){
			tickPrice[i] = (double) stock.pastPrices.get(i);
		}
		
				
		// Declaration of macdExt:
		// -----------------------
		/*
   			public RetCode stochRsi( int startIdx,
      		int endIdx,
      		double inReal[],
      		int optInTimePeriod,
      		int optInFastK_Period,
      		int optInFastD_Period,
      		MAType optInFastD_MAType,
      		MInteger outBegIdx,
      		MInteger outNBElement,
      		double outFastK[],
      		double outFastD[] )
		*/
		

		srsiRcore = srsi.stochRsi(startIndex, endIndex, tickPrice,SRSICfg.srsiSimTicksParam, 
				SRSICfg.srsiFastKParam,SRSICfg.srsiFastDParam, SRSICfg.srsiMATypeDParam, 
				outBegIdx, outNBElement, outSRSIK, outSRSID);
		int flag = 0;
		for (int i = 0 ; i < outSRSIK.length ; i++){
			if (outSRSIK[i] > 0){
				flag=1;
			}
		}
		
		if (flag == 0){
			System.out.println("check Data coherency on: " + stock.ticker);
		}
		
		
		if ( srsiRcore == RetCode.Success){		
			
			double result=0;
			if (flag == 1){
				result = getGrade(outSRSIK,outSRSID,operationType);
			}
			if (result > 0){
				stock.indicatorsTable.put("SRSI", true);
			} else {
				stock.indicatorsTable.put("SRSI", false);
			}
			
			return result;
		} else {
			// Algorithm failed:
			// ----------------
			System.err.println("StochRSI algorithm failed for Stock - " + stock.ticker);
			return 0;
		}
	
	}
	
	private double getGrade(double[] srsiK,double[] srsiD,OPType operationType){
		
		int result = 0;
		double lastSRSID = srsiD[0];
		
		if (operationType == OPType.BUY){
			
			// buy:
			// ----
			// 1. if last SRSID smaller than BuySRSIParam, buy
			
			
			if (lastSRSID < SRSICfg.srsiBuyParam ){
				
				result = 1;

			}			
		
		} else {
			
			// Sell:
			// -----
			// 1. if last SRSID is bigger than SellSRSIDParam, sell
			if (lastSRSID > SRSICfg.srsiSellParam ){
				
				result = 1;
				
			}
			
		}
	
		return result;
	}
	
	
}
