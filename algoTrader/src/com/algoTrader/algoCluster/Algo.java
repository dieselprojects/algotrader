package com.algoTrader.algoCluster;

import com.algoTrader.controllerCluster.StockBean;
import com.algoTrader.supportCluster.Types.OPType;

public abstract class Algo {

	protected abstract double start(StockBean stock, OPType operationType);

}
