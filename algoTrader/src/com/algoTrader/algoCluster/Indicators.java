package com.algoTrader.algoCluster;

import com.algoTrader.controllerCluster.StockBean;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;
import com.algoTrader.supportCluster.IndicatorsCfg;
import com.algoTrader.supportCluster.Types.OPType;

public class Indicators extends Algo{

	
	// Use TA-Lib Buy/Bullish Indicators:
	//CDL3INSIDE          Three Inside Up // Weak
	//CDL3OUTSIDE         Three Outside Up // GOOD
	//CDL3STARSINSOUTH    Three Stars In The South // Strongest
	//CDL3WHITESOLDIERS   Three Advancing White Soldiers// Strong 
	//CDLABANDONEDBABY    Abandoned Baby // Strong

	
	
	
	
	public double start(StockBean stock, OPType operationType) {
		
            IndicatorsCfg.getInstance();
	    
            // Grade factors:
	    // --------------
	    int cdl3InsideGradeFactor        = IndicatorsCfg.cdl3InsideGradeFactor      ;
	    int cld3outsideGradeFactor       = IndicatorsCfg.cld3outsideGradeFactor     ;
	    int cld3StarsInSouthGradeFactor  = IndicatorsCfg.cld3StarsInSouthGradeFactor;
	    int cld3WhiteSolGradeFactor      = IndicatorsCfg.cld3WhiteSolGradeFactor    ;
		// --------------------------- //
		// - Buy and Sell Indicators - //
		// --------------------------- //
		
		double[] openPrices 	= stock.getListAsArray(stock.pastOpen); 
		double[] highPrices	 	= stock.getListAsArray(stock.pastHigh);
		double[] lowPrices 		= stock.getListAsArray(stock.pastLow);
		double[] closePrices 	= stock.getListAsArray(stock.pastClose);
		int startIndex  		= stock.pastClose.size() - 10;
		int endIndex			= stock.pastClose.size() - 1;
		int size				= stock.pastClose.size();
		
		int indicationIndex = endIndex - startIndex;
		
		// Three Inside Up:
		double gradeThreeInside 	= getThreeInside(openPrices,highPrices,lowPrices,closePrices,startIndex,endIndex,indicationIndex,size,operationType);
		double gradeThreeOutside	= getThreeOutside(openPrices,highPrices,lowPrices,closePrices,startIndex,endIndex,indicationIndex,size,operationType);
		double gradeThreeStars		= getThreeStars(openPrices,highPrices,lowPrices,closePrices,startIndex,endIndex,indicationIndex,size,operationType);
		double gradeThreeSoldiers	= getThreeSoldiers(openPrices,highPrices,lowPrices,closePrices,startIndex,endIndex,indicationIndex,size,operationType);

		
		// Setting Indicators table:
		// -------------------------
		
		if (gradeThreeInside > 0){
			stock.indicatorsTable.put("ThreeInside", true);
		} else {
			stock.indicatorsTable.put("ThreeInside", false);
		}
		
		if (gradeThreeOutside > 0){
			stock.indicatorsTable.put("ThreeOutside", true);
		} else {
			stock.indicatorsTable.put("ThreeOutside", false);
		}
		
		if (gradeThreeStars > 0){
			stock.indicatorsTable.put("ThreeStars", true);
		} else {
			stock.indicatorsTable.put("ThreeStars", false);
		}
		
		if (gradeThreeSoldiers > 0){
			stock.indicatorsTable.put("ThreeSoldiers", true);
		} else {
			stock.indicatorsTable.put("ThreeSoldiers", false);
		}
		
		
		double Grade = gradeThreeInside 	*	cdl3InsideGradeFactor 		 
						+ gradeThreeOutside	*   cld3outsideGradeFactor 		
		                + gradeThreeStars	*	cld3StarsInSouthGradeFactor 
		                + gradeThreeSoldiers*   cld3WhiteSolGradeFactor 	;
		
		return Grade;
	}
	
	
	
	// Indicators:
	// -----------
	
	// ---------------- //
	// - Three Inside - //
	// ---------------- //
	
	private double getThreeInside(double[] openPrices,
									double[] highPrices,
									double[] lowPrices,
									double[] closePrices,
									int startIndex,
									int endIndex,
									int indicationIndex,
									int size,
									OPType operationType){
	
		RetCode tiuRcore;
		Core tiu = new Core();
		MInteger outBegIdx		= new MInteger();
	    MInteger outNBElement	= new MInteger();
	    
	    
		int[] outInt = new int[size];
		
		tiuRcore = tiu.cdl3Inside(startIndex, endIndex, openPrices, highPrices, lowPrices, closePrices, outBegIdx, outNBElement, outInt);
		

		
		if (tiuRcore != RetCode.Success){ System.err.println("ThreeInside Failed"); };

		if (operationType == OPType.BUY)
			return ( outInt[indicationIndex] == 100 ) ? 1 : 0;
		else
			return ( outInt[indicationIndex] == -100) ? 1 : 0;
		
	}
	
	// ----------------- //
	// - Three OutSide - //
	// ----------------- //
	
	private double getThreeOutside(double[] openPrices,
									double[] highPrices,
									double[] lowPrices,
									double[] closePrices,
									int startIndex,
									int endIndex,
									int indicationIndex,
									int size,
									OPType operationType){
	
		RetCode tiuRcore;
		Core tiu = new Core();
		MInteger outBegIdx		= new MInteger();
	    MInteger outNBElement	= new MInteger();
		
		
		int[] outInt = new int[size];
		
		tiuRcore = tiu.cdl3Outside(startIndex, endIndex, openPrices, highPrices, lowPrices, closePrices, outBegIdx, outNBElement, outInt);
		
		if (tiuRcore != RetCode.Success){ System.err.println("ThreeOutSide Failed"); };

		if (operationType == OPType.BUY)
			return ( outInt[indicationIndex] == 100 ) ? 1 : 0;
		else
			return ( outInt[indicationIndex] == -100) ? 1 : 0;
		
	}
	
	
	// --------------- //
	// - Three Stars - //
	// --------------- //
	
	private double getThreeStars(double[] openPrices,
									double[] highPrices,
									double[] lowPrices,
									double[] closePrices,
									int startIndex,
									int endIndex,
									int indicationIndex,
									int size,
									OPType operationType){
	
		RetCode tiuRcore;
		Core tiu = new Core();
		MInteger outBegIdx		= new MInteger();
	    MInteger outNBElement	= new MInteger();
		
		
		int[] outInt = new int[size];
		
		tiuRcore = tiu.cdl3StarsInSouth(startIndex, endIndex, openPrices, highPrices, lowPrices, closePrices, outBegIdx, outNBElement, outInt);
		
		if (tiuRcore != RetCode.Success){ System.err.println("Three Stars in South Failed"); };

		if (operationType == OPType.BUY)
			return ( outInt[indicationIndex] == 100 ) ? 1 : 0;
		else
			return ( outInt[indicationIndex] == -100) ? 1 : 0;
		
	}
	
	// --------------- //
	// - Three Soldiers - //
	// --------------- //
	
	private double getThreeSoldiers(double[] openPrices,
									double[] highPrices,
									double[] lowPrices,
									double[] closePrices,
									int startIndex,
									int endIndex,
									int indicationIndex,
									int size,
									OPType operationType){
	
		RetCode tiuRcore;
		Core tiu = new Core();
		MInteger outBegIdx		= new MInteger();
	    MInteger outNBElement	= new MInteger();
		
		
		int[] outInt = new int[size];
		
		tiuRcore = tiu.cdl3WhiteSoldiers(startIndex, endIndex, openPrices, highPrices, lowPrices, closePrices, outBegIdx, outNBElement, outInt);
		
		if (tiuRcore != RetCode.Success){ System.err.println("Three White Soldiers Failed"); };

		if (operationType == OPType.BUY)
			return ( outInt[indicationIndex] == 100 ) ? 1 : 0;
		else
			return ( outInt[indicationIndex] == -100) ? 1 : 0;
		
	}

}
