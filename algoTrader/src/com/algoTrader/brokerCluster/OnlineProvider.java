package com.algoTrader.brokerCluster;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

public interface OnlineProvider {
	

	public String OPname = "";
	
	public String getName();
	
	// ------------------ //
	// - Request Prices - //
	// ------------------ //
	
	public ArrayList<Tick> requestPrices(String[] tickers,BrokerTickType brokerTickType);
	
	
	// ------------- //
	// - Place Bid - //
	// ------------- //
	
	public Bid placebid(Bid bid);
	

	// ---------------- //
	// - getPastTicks - //
	// ---------------- //
	
	public algoTesterBean getPastTicks(String ticker, String startDate, String endDate, String sample) throws IOException, ParseException;

}
