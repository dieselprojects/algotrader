package com.algoTrader.brokerCluster;

import java.util.Calendar;

public class PriceTick extends Tick{
	
	public Calendar priceDate = null;
	public Long volume;
			
	// Constructor for Simulated trade 
	
	public PriceTick(String ticker, Double price){
		super(ticker, price);

	}
	
	// Constructor for online trade, with Date
	
	public PriceTick(String ticker, Double price, Long volume, Calendar priceDate){
		super(ticker,price);
		this.priceDate = priceDate;
		this.volume = volume;
	}
}
