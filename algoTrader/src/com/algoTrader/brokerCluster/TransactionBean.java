package com.algoTrader.brokerCluster;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;

import com.algoTrader.supportCluster.Configuration;
import com.algoTrader.supportCluster.Types.OPType;

public class TransactionBean {
	String 		ticker;
	OPType		transactionOPType;		
	Double 		transactionPrice;
	Integer 	transactionAmount;
	Calendar 	transactionDate;
	
	LinkedList<String> transactionIndicators;
	LinkedList<Double> transactionIndicatorsGrades;
	Double		transactionTotalGrade;
	
	// for sell transaction only, TransactionBean will calculate profit according
	Double  	transactionBuyPrice;
	Integer		transactionBuyAmount;
	String		transactionProfit;
	String		transactionProfitPercent;
	boolean 	profit;
	
	Configuration configuration = Configuration.getInstance();
	
	// Logging colors:
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";	
	public static final String ANSI_BOLD  = "\u001b[1m";
	
	
	
	
	// Buy constructor
	public TransactionBean( 
				String 		ticker,
				OPType		transactionOPType,		
				Double 		transactionPrice,
				Integer 	transactionAmount,
				Calendar 	transactionDate,
				Double		transactionTotalGrade
	){
		this.ticker 				= ticker 				;
		this.transactionOPType 		= transactionOPType 	;
		this.transactionPrice 		= transactionPrice 		;
		this.transactionAmount 		= transactionAmount 	;
		this.transactionDate 		= transactionDate 		;
		this.transactionTotalGrade	= transactionTotalGrade ;
		
	}

	// Sell constructor
	public TransactionBean( 
				String 		ticker,
				OPType		transactionOPType,		
				Double 		transactionPrice,
				Integer 	transactionAmount,
				Calendar 	transactionDate,
				Double		transactionTotalGrade,
				Double		transactionBuyPrice,
				Integer		transactionBuyAmount
	){
		this.ticker 				= ticker 				;
		this.transactionOPType 		= transactionOPType 	;
		this.transactionPrice 		= transactionPrice 		;
		this.transactionAmount 		= transactionAmount 	;
		this.transactionDate 		= transactionDate 		;
		this.transactionTotalGrade	= transactionTotalGrade ;
		this.transactionBuyPrice	= transactionBuyPrice	;
		this.transactionBuyAmount	= transactionBuyAmount	;
		
		// calculate profit:
		Double profitPercent = (transactionPrice/transactionBuyPrice - 1.0)*100.0;
		transactionProfitPercent = ( profitPercent > 0.0 ) ? new DecimalFormat(".##").format(profitPercent) + "%" 
												: "(" + new DecimalFormat(".##").format(profitPercent) + "%)";
		Double profitDollar = (transactionPrice - transactionBuyPrice ) * transactionAmount;
		transactionProfit = ( profitDollar > 0 ) ? new DecimalFormat(".##").format(profitDollar) + "$"
												:  "(" + new DecimalFormat(".##").format(profitDollar) + "$)";
		profit = (profitDollar > 0);
	}
	
	
	public String print(){
		String returnString = "";
		String colorString = "";
		
		// parse date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		String date =  sdf.format(transactionDate.getTime());
		
		returnString += "-------------------------------------------------------------\n";
		
		// Color for transaction profit
		if ( configuration.enableColor == true ){
			
			if (transactionOPType ==  OPType.SELL ){
				
				if ( profit == true ){
					colorString += ANSI_BOLD + ANSI_GREEN;
				} else {
					colorString += ANSI_BOLD + ANSI_RED;
				}
			}
			
		}	
		returnString += colorString + date + "# " + ticker + ", " + transactionOPType + ", Price: " + 	transactionPrice + ", Amount: " + 	transactionAmount +  ANSI_RESET + "\n";
		returnString += colorString;
		returnString += (transactionOPType == OPType.SELL) ? "Profit: " + transactionProfitPercent + ", "  + transactionProfit +  ANSI_RESET + "\n\n" : "\n";  
	
		// TODO: Add indicators
		
		return returnString;
	}
	
}
