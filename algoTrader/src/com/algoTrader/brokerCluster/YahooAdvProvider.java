package com.algoTrader.brokerCluster;

import java.util.Map;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;


public class YahooAdvProvider {
	
	public Stock getStock(String ticker, boolean history){
		return YahooFinance.get(ticker,history);
	}

	public Map<String, Stock> getStocks(String[] tickers, boolean history){
		return YahooFinance.get(tickers,history);
	}

}
