package com.algoTrader.brokerCluster;

import com.algoTrader.controllerCluster.StockBean;

public class Bid {
	
	// StateMachine approves:
	// ----------------------
	public Boolean			approvedHuman;
	public Boolean			approvedBroker;
	
	
	public String 			ticker	;
	public Double 			price	;
	public int 				amount	;
	public BrokerBidType 	bidType	;
	public StockBean		stock	;
	
	
	
	public Bid(String ticker,	
				Double price,	
				int amount,	
				BrokerBidType bidType,
				StockBean stock){
		this.ticker	 = ticker	;
		this.price	 = price	;
		this.amount	 = amount	;
		this.bidType = bidType  ;
		this.stock	 = stock	;
		
		this.approvedHuman 	= false;
		this.approvedBroker = false;
	}
	
	
	public String print(){
		String bidString = "Bid Summery for ticker: " + ticker +"\n";
		bidString += "Request: " + bidType.toString() + ", Price: " + price + ", Amount: " + amount + "\n";
		bidString += "--------------------------------------------\n";
		bidString += "General stock Information:\n";
		bidString += stock.printHuman();		
		
		return bidString;
	}
}
