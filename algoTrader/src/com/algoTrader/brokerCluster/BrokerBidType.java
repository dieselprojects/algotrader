package com.algoTrader.brokerCluster;

public enum BrokerBidType {
	Buy,
	Sell
}
