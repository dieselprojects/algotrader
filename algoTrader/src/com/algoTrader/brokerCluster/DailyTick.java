package com.algoTrader.brokerCluster;

import java.util.Calendar;

public class DailyTick extends Tick{

	
	public Double 	openPrice;
	public Double 	closePrice;
	public Double 	highPrice;
	public Double 	lowPrice;
	public Calendar	dailyDate;
	public Long 	volume;
	
	
	// Constructor for online trade, added Date
	
	public DailyTick(String 	ticker,
			Double 		price,
			Double 		openPrice,
			Double 		highPrice,
			Double 		lowPrice,
			Double 		closePrice,
			Long		volume,
			Calendar 	dailyDate
			
	){
		super(ticker, price);
		this.openPrice	= openPrice	;
		this.closePrice	= closePrice;
		this.highPrice	= highPrice	;
		this.lowPrice	= lowPrice	;
		this.dailyDate	= dailyDate	;

	}
	
	// Constructor for simulated trade
	
	public DailyTick(String 	ticker,
			Double 		price,
			Double 		openPrice,
			Double 		closePrice,
			Double 		highPrice,
			Double 		lowPrice
	){
			super(ticker, price);
			this.openPrice	= openPrice	;
			this.closePrice	= closePrice;
			this.highPrice	= highPrice	;
			this.lowPrice	= lowPrice	;
	}
	
}
