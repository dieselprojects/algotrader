package com.algoTrader.brokerCluster;

public abstract class Tick {
	
	public String ticker;
	public Double price;
	
	public Tick(String ticker, Double price){
		this.ticker = ticker;
		this.price = price;
	}
}
