package com.algoTrader.brokerCluster;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import com.algoTrader.supportCluster.Configuration;
import com.algoTrader.supportCluster.FundsMan;
import com.algoTrader.supportCluster.Logger;
import com.algoTrader.supportCluster.Types.OPType;

// Simulated Broker:
// -----------------

// General:
// --------

// Handle simulated tick and prices 
// holds 


// Singleton implementation:	

public class SimulatedBroker implements BrokerIF{

	// ---------------------------- //
	// - Singleton implementation - //
	// ---------------------------- //
	
	private static final SimulatedBroker broker = new SimulatedBroker();
	
	public static SimulatedBroker getInstance(){
		return broker;
	}
	
	private OnlineProvider onlineProvider;
	Configuration configuration;
	int Iteration;
	int EndOfTest;
	BufferedReader FilteredReader;
	ArrayList <algoTesterBean> testerArray;
	FundsMan funds;
	private Logger logger = Logger.getInstance();;
	
		
	// --------------- //
	// - Constructor - //
	// --------------- //
	
	private SimulatedBroker(){
		
		System.out.println("__Constructor__ Simulated Broker, Creating Simulated Data");
				
		// ---------------------------- //
		// - Creating Stimulated Data - //
		// ---------------------------- //
		
		configuration = Configuration.getInstance();
		switch (configuration.providerType){
		case YAHOO_PROVIDER:
			System.out.println("Creating onlineProvider, providerType - Yahoo provider");
			onlineProvider =  new YahooProvider();
			break;
		default:
			System.err.println("Invalid provider type selection");
			System.err.println("Exiting...");
			System.exit(1);
			break;
		}
		
		// Date parameters:
		String startDate = configuration.startDate;
		String endDate   = configuration.endDate;
		String sample 	 = configuration.sample;
		
		
		
		// Array list of Stock Prices overTime:
		// ------------------------------------
		testerArray= new ArrayList<algoTesterBean>();
		
		
		// Generating d:
		// -------------------------------------
		// Each stock will create a new LinkedList and the Linked list will be added to the ArrayList in a new place
		
		try{
			
			FilteredReader = new BufferedReader(new FileReader("Res/filteredStocks.csv"));
			String ticker = "";
	
			while ( (ticker = FilteredReader.readLine()) != null){
				
				ticker = ticker.replaceAll("\"", "");
				
				testerArray.add( onlineProvider.getPastTicks(ticker,startDate,endDate,sample) );
			
				// 100 ms wait until next request due to overload constraint on Yahoo CSV API
				Thread.sleep(100);
				System.out.println(ticker);
			}
			FilteredReader.close();	
		
		} catch( Exception e ){
			
			System.err.println("Simulate Broker, Constructor Read File Error");
			System.exit(1);
		
		}
		
		
		
		// Setting class Variables:
		// ------------------------
		
		Iteration = 0;
		EndOfTest = testerArray.get(0).prices_len;
		
		// Getting funds singleton:
		// ------------------------
		funds = FundsMan.getInstance();
		
		System.out.println("Simulated Broker Constructor, Done Creating Simulated Data, Total of planed Iteration until EOT: " + EndOfTest);
	}
	
	// ------------- //
	// - Get Tick  - //
	// ------------- //
	
	// General: 
	// --------
	// generate Daily or Price tick in respect to Request.
	// SimulatedBroker can generate small delays.
	// Tick class is being generated
	
	public Tick getTick(String ticker, BrokerTickType brokerTickType) {
		
		if (brokerTickType == BrokerTickType.DailyTick){
			
			Tick dailyTick = null;
			
			// find ticker in testerArray:
			// ---------------------------
			for (int i = 0 ; i < testerArray.size() ; i++ ){
				if (testerArray.get(i).ticker == ticker){
					dailyTick = new DailyTick(testerArray.get(i).ticker,
												testerArray.get(i).priceList.get(Iteration),
												testerArray.get(i).pastOpen.get(Iteration),
												testerArray.get(i).pastClose.get(Iteration),
												testerArray.get(i).pastHigh.get(Iteration),
												testerArray.get(i).pastLow.get(Iteration));
				}
			}
			
			if (dailyTick != null){
				return dailyTick;
			} else{
				System.err.println("Ticker not Found in Tester Array: " + ticker);
				System.exit(1);			
			}
			
			
		} else {
			System.err.println("priceTick currentlly not supported");
			System.exit(1);
		}
		
		return null;
		
		
	}



	
	public ArrayList<Tick> getTick(ArrayList<String> tickerArray,
			BrokerTickType brokerTickType) {
		
		ArrayList<Tick> returnedArray = new ArrayList<Tick>();
		
		for (String nextTicker : tickerArray){
			returnedArray.add( (DailyTick) getTick(nextTicker,brokerTickType));
		}
		
		
		if (configuration.simulatedDelay){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.err.println("__ERR__ Simulated Delay failed, Broker.getTick");
				System.exit(1);
			}
		}
		
		return returnedArray;

	}

	
	// ------------- //
	// - Place Bid - //
	// ------------- //
	
	// Trade Holding useCase:
	//	d. funds get the DoneBids and removeFunds according to the doneBids
	//	e. funds use DoneBid to update the doneBid.Stock
		
	public Bid placeBid(Bid bid) {
		
		Boolean bidSuccess ;
		Bid doneBid = bid;
		
		
		if (configuration.simulatedDelay){
			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				System.err.println("__ERR__ Simulated Delay failed, Broker.bid");
				System.exit(1);
			}
		}
		
		//TODO: add simulated success/fail bids
		bidSuccess = true;
		
		if (bidSuccess == true){
		
			if ( bid.bidType == BrokerBidType.Buy){
				
				// BuyBid:
				if ( funds.purchaseStock(bid.ticker, bid.price, bid.amount) ){
					
					// got funds to buy
					doneBid.stock.currentStatus = true;
					doneBid.stock.investedAmount += bid.amount;
					doneBid.stock.buyPrice = bid.price;
		
					// TODO: calculate date from iteration:
					Calendar transactionDate = new GregorianCalendar();
					
					// Store transaction in transaction queue:
					TransactionBean transactionBean = new TransactionBean(
								bid.ticker,
								OPType.BUY,
								bid.price,
								bid.amount,
								transactionDate,
								bid.stock.grade			
					);
					logger.addTransaction(transactionBean);				
					
					
				} else {
					System.out.println("__ERR__ During buy, funds error");
					System.exit(1);
				}
			
			} else {
				// Sell Bid:
				if (funds.sellStock(bid.ticker, bid.price, bid.amount, bid.stock.buyPrice)){
				
					
					// TODO: calculate date from iteration:
					Calendar transactionDate = new GregorianCalendar();
					// Store transaction in transaction queue:
					TransactionBean transactionBean = new TransactionBean(
								bid.ticker,
								OPType.SELL,
								bid.price,
								bid.amount,
								transactionDate,
								bid.stock.grade,
								bid.stock.buyPrice,
								bid.stock.investedAmount
					);
					logger.addTransaction(transactionBean);				
					
					// funds returned OK
					doneBid.stock.currentStatus = false;
					doneBid.stock.investedAmount -= bid.amount;
					
				} else {
					System.out.println("__ERR__ During Sell, funds error");
					System.exit(1);				
				}
				
			}			
		
			// Indicate Broker done:
			doneBid.approvedBroker = true;
			
		}
		
		return doneBid;
	}

	
	// -------------- //
	// - Place Bids - //
	// -------------- //
		
	// Sale useCase:
	// -------------
	//	d. funds get the DoneBids and removeFunds according to the doneBids
	
	
	public LinkedList<Bid> placeBids(LinkedList<Bid> bids) {
		
		LinkedList<Bid> doneBids = new LinkedList<Bid>();
		
		for (Bid nextBid : bids){
			
			Bid doneBid = placeBid(nextBid);
			
			if (doneBid.approvedBroker == true){
				doneBids.addLast(doneBid);
			}		
		}

		return doneBids;
		
		// TODO: Add Logger!! - no need for Funds logger, this is the Done Bids
	}
	
	
	// ----------------- //
	// - getAllTickers - //
	// ----------------- //
	
	public ArrayList<String> getAllTickers(Boolean getTickersFromFile) {
		ArrayList<String> tickerArray = new ArrayList<String>(testerArray.size());
		
		int i = 0;
		for (algoTesterBean Bean : testerArray){
			tickerArray.add(i,Bean.ticker);
			i++;
		}
		
		return tickerArray;
	}
	
	
	
	
	// Advance iteration number for simulated broker
	public Boolean advIteration(){
		Iteration++;
		return (Iteration != EndOfTest);
		
	}
	
	// TODO: future implementation can generate a simulated date and not iteration
	// this will be before the uploading
	public int getIteration(){ return Iteration; }

}
