package com.algoTrader.brokerCluster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import com.algoTrader.supportCluster.Configuration;
import com.algoTrader.supportCluster.FundsMan;

public class YahooProvider implements OnlineProvider{

	Configuration configuration = Configuration.getInstance();
	FundsMan funds = FundsMan.getInstance();
	
	public String OPname = "Yahoo Provider";
	
	public String getName(){
		return OPname;
	}
	
	public ArrayList<Tick> requestPrices(String[] tickers, BrokerTickType brokerTickType) {
		
		// create a request for yahoo with all given ticker - up to 150 tickers at a time
		// send request for yahoo for current prices or daily prices
		// Called should not send a request for more than 100 tickers
		// Added implementation for simulated ticks (dailyTicks only)
		
		ArrayList<Tick> returnedTickList = new ArrayList<Tick>();
		
		String requestOperands = (brokerTickType == BrokerTickType.PriceTick) ?
					"sl1v" // Ticker, latest price, volume
				:
					"sl1ohgl1v"; // Ticker, price, open, high, low, close, volume;
		
		
		String tickerString = "";
		for (String nextTicker : tickers){
			tickerString += "+" +nextTicker;
		}
		
		// remove first '+'
		tickerString = tickerString.substring(1);

		try {
			
			// Generate yahoo price call:
			// -------------------------- 
			
			URL yahooPricesCall;
			yahooPricesCall = new URL("http://finance.yahoo.com/d/quotes.csv?s=" + tickerString + "&f=" + requestOperands + "&ignore=.csv");
		
			URLConnection yc = yahooPricesCall.openConnection();
		
			// Read Input Stream:
			BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
	    
			String pricesLine = "";
			while ( (pricesLine = in.readLine()) != null ){
				String[] splitString = pricesLine.split(",");
			
				// Current request time
				Calendar currentTime = new GregorianCalendar();
			
				Tick nextPriceTick;
				
				if (brokerTickType == BrokerTickType.PriceTick){
				
					// New Price Tick:
					// ---------------
					
					nextPriceTick = new PriceTick(splitString[0],
							Double.valueOf(splitString[1]),
							Long.valueOf(splitString[2]),
							currentTime);
			
				} else {
					
					// New Daily Tick:
					// ---------------
					
					nextPriceTick = new DailyTick(splitString[0], // ticker
								Double.valueOf(splitString[1]), // price
								Double.valueOf(splitString[2]), // open
								Double.valueOf(splitString[3]), // high
								Double.valueOf(splitString[4]), // low
								Double.valueOf(splitString[5]), // close
								Long.valueOf(splitString[6]),   // volume
								currentTime); // current time			
				}
				
				// Add to returned List
				returnedTickList.add(nextPriceTick);				
			}
		} catch (MalformedURLException e) {			
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);				
		}

		
		return returnedTickList;
	}

	// Yahoo provider place simulated Online bids:
	public Bid placebid(Bid bid) {
		
		Boolean bidSuccess;
		Bid doneBid = bid;
		
		// Get random simulated delay
		if (configuration.simulatedDelay == true){
			try {
				Integer randDelay = (int) Math.random()*200;
				Thread.sleep(randDelay);
			} catch (InterruptedException e) {
				System.err.println("__ERR__ Simulated Delay failed, Broker.bid");
				System.exit(1);
			}
		}
		
		// 90% bid success:
		bidSuccess = (Math.random() < 0.9) ? true :false;
		
		if (bidSuccess == true){
			
			if ( bid.bidType == BrokerBidType.Buy){
				// BuyBid:
				if ( funds.purchaseStock(bid.ticker, bid.price, bid.amount) ){
					
					// got funds to buy
					
					doneBid.stock.currentStatus = true;
					doneBid.stock.investedAmount += bid.amount;
					doneBid.stock.buyPrice = bid.price;
					
					
				} else {
					System.out.println("__ERR__ During buy, funds error");
					System.exit(1);
				}
				

				
			
			} else {
				// Sell Bid:
				if (funds.sellStock(bid.ticker, bid.price, bid.amount, bid.stock.buyPrice)){
					
					// funds returned OK
					
					doneBid.stock.currentStatus = false;
					doneBid.stock.investedAmount -= bid.amount;
					
				} else {
					System.out.println("__ERR__ During Sell, funds error");
					System.exit(1);				
				}
				
			}			
		
			// Indicate Broker done:
			doneBid.approvedBroker = true;
			
		} else {
			System.out.println("Bid Failed on " + doneBid.ticker + " for type " + doneBid.bidType);
		}		
		
		return doneBid;
	}
	
	// ---------------------------------------------------------------- //
	// - CALL YAHOO, for each ticker, calls yahoo for all past prices - //
	// ---------------------------------------------------------------- //	
	
	public algoTesterBean getPastTicks(String ticker, String startDate, String endDate, String sample) throws IOException, ParseException{
		algoTesterBean testerBean;
		
		// setting Date for Yahoo call:
		String[] sSD = startDate.split("\\.");
		String[] sED = endDate.split("\\.");
		
		// Instantiate prices List:  
		LinkedList<Double> priceList 		= new LinkedList<Double>();
		LinkedList<Double> openPriceList 	= new LinkedList<Double>();
		LinkedList<Double> highPriceList 	= new LinkedList<Double>();
		LinkedList<Double> lowPriceList 	= new LinkedList<Double>();
		LinkedList<Double> closePriceList	= new LinkedList<Double>();
		
		// Generating URL req for Yahoo:
		URL yahooPricescall = new URL("http://ichart.yahoo.com/table.csv?s=" + ticker + 
				"&a=" +sSD[1] + // Start Month - 1
				"&b=" +sSD[0] + // Start Day
				"&c=" +sSD[2] + // Start Year
				"&d=" +sED[1] + // End Month - 1
				"&e=" +sED[0] + // End Day
				"&f=" +sED[2] + // End Year
				"&g=" + sample + "&ignore=.csv "); // Sample in days

		// Open URL connection:
		URLConnection yc = yahooPricescall.openConnection();
        
		// Read Input Stream:
		BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
        
		// first line isn't prices - getting the next line
        String pricesLine = in.readLine();
        
        while ( (pricesLine = in.readLine()) != null){
        	String[] splitString = pricesLine.split(",");

        	// Accumulate all prices in the test, later this prices will be send one by one (like in the real world)
        	// -----------------------------------------------------------------------------------------------------
        	// Open:
        	openPriceList.addFirst(Double.valueOf(splitString[1]));
        	// High:
        	highPriceList.addFirst(Double.valueOf(splitString[2])); 
        	// Low:
        	lowPriceList.addFirst(Double.valueOf(splitString[3])); 
        	// Close:
        	closePriceList.addFirst(Double.valueOf(splitString[4]));
        	priceList.addFirst(Double.valueOf(splitString[4]));
        	
        }
        
        testerBean = new algoTesterBean(ticker,priceList,openPriceList,highPriceList,lowPriceList,closePriceList);
		return testerBean;
	}
	

}
