package com.algoTrader.brokerCluster;

import java.util.LinkedList;

public class algoTesterBean {
	String ticker;
	LinkedList<Double> priceList;
	LinkedList<Double> pastClose;
	LinkedList<Double> pastHigh	;
	LinkedList<Double> pastLow	;
	LinkedList<Double> pastOpen	;
	
	int prices_len;
	
	algoTesterBean(String ticker,
			
			LinkedList<Double> priceList,
			LinkedList<Double> pastClose,	
			LinkedList<Double> pastHigh,	
			LinkedList<Double> pastLow,	
			LinkedList<Double> pastOpen
			
			){
		this.ticker = ticker;
		this.priceList = priceList;
		this.pastClose = pastClose;	
		this.pastHigh  = pastHigh ;	
		this.pastLow   = pastLow  ;
		this.pastOpen  = pastOpen ;
		this.prices_len = priceList.size();
	}
}
