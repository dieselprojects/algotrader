package com.algoTrader.brokerCluster;

import java.util.ArrayList;
import java.util.LinkedList;


public interface BrokerIF {
	
	
	// ---------------------------- //
	// - Get Tick (single ticker) - //
	// ---------------------------- //
	
	public Tick getTick(String ticker, BrokerTickType brokerTickType);
	
	
	// ------------------------------- //
	// - Get Tick (By ticker vector) - //
	// ------------------------------- //
	
	// Call Broker and Get Tick information: PriceTick - Current price tick Information, 
	// Daily Tick Last Trade Day prices Information
	// Input: ArrayList<String> tickers
	// Output: ArrayList<DailyTick>
	
	public ArrayList<Tick> getTick(ArrayList<String> tickerArray, BrokerTickType brokerTickType);
	
	
	// ------------- //
	// - Place Bid - //
	// ------------- //
	
	// Place Single bid
	public Bid placeBid(Bid bid);
	
	// -------------- //
	// - Place Bids - //
	// -------------- //
	
	// Place arrayList of bids
	public LinkedList<Bid> placeBids(LinkedList<Bid> bids);
	
	
	// --------------------- //
	// - Get StocksTickers - //
	// --------------------- //
	
	public ArrayList<String> getAllTickers(Boolean getTickersFromFile);
	
	
	
}
