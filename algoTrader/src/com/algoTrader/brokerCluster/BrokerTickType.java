package com.algoTrader.brokerCluster;

public enum BrokerTickType {
	DailyTick,
	PriceTick
}
