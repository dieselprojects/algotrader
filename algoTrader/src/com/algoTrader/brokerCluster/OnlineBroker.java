package com.algoTrader.brokerCluster;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;

import com.algoTrader.supportCluster.Configuration;
import com.algoTrader.supportCluster.Logger;

public class OnlineBroker implements BrokerIF {

	private static final OnlineBroker broker = new OnlineBroker();
	
	public static OnlineBroker getInstance(){
		return broker;
	}
	
	private OnlineProvider onlineProvider;
	
	private Configuration configuration = Configuration.getInstance();
	private Logger logger = Logger.getInstance();
	
	// --------------- //
	// - Constructor - //
	// --------------- //	
	
	private OnlineBroker(){
		
		System.out.println("__Constructor__ Online Broker");
		switch (configuration.providerType){
		case YAHOO_PROVIDER:
			System.out.println("Creating onlineProvider, providerType - Yahoo provider");
			onlineProvider =  new YahooProvider();
			break;
		default:
			System.err.println("Invalid provider type selection");
			System.err.println("Exiting...");
			System.exit(1);
			break;
		}
		
		logger.addEvent("__Constructor__ Online Broker, using online provider - " + onlineProvider.getName());		
	
	}
	
	
	// --------------------------------------- //
	// - Get Tick Prices & Tick prices Array - //
	// --------------------------------------- //
	
	public Tick getTick(String ticker, BrokerTickType brokerTickType) {
		String[] tickers = {ticker};
		return onlineProvider.requestPrices(tickers, brokerTickType).get(0);
	}

	
	public ArrayList<Tick> getTick(ArrayList<String> tickerArray,
			BrokerTickType brokerTickType) {
		
		// Convert ArrayList<String> to String
		String[] tickerArr = new String[tickerArray.size()];
		tickerArr = tickerArray.toArray(tickerArr);

		return onlineProvider.requestPrices(tickerArr, brokerTickType);
	}

	
	//	------------------------- //
	// - Place Bid & Bids Array - //
	// -------------------------- //
	
	public Bid placeBid(Bid bid) {
		
		return onlineProvider.placebid(bid);
	}

	
	public LinkedList<Bid> placeBids(LinkedList<Bid> bids) {
		
		LinkedList<Bid> returnedBidsArray = new LinkedList<Bid>();
		
		for (Bid nextBid : bids){
			returnedBidsArray.add(placeBid(nextBid));
		}
		
		return returnedBidsArray;
	}

	
	// ----------------------------- //
	// - Get all tickers by String - //
	// ----------------------------- //
	
	public ArrayList<String> getAllTickers(Boolean getTickersFromFile) {
		
		ArrayList<String> tickerArray = new ArrayList<String>();
		
		if (getTickersFromFile == true){
			
			// Loading Filtered stock list and storing in tickerArray
			try{
				
				BufferedReader FilteredReader = new BufferedReader(new FileReader(configuration.filteredStockFile));
				String tickerString = "";
			
				while ( (tickerString = FilteredReader.readLine()) != null){
								
					tickerString = tickerString.replaceAll("\"", "");
					tickerArray.add(tickerString);

				}
				FilteredReader.close();	
			
			} catch( Exception e ){
				e.printStackTrace();
				System.exit(1);
			}
			
		} else{
			System.err.println("Should not get tickers from broker! ");
			System.exit(1);
		}

		return tickerArray;
	}


}
