package com.algoTrader.supportCluster;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import com.algoTrader.brokerCluster.BrokerIF;
import com.algoTrader.brokerCluster.TransactionBean;
import com.algoTrader.supportCluster.Types.ReportType;

public class Logger {
	
	
	// Logger singleton:
	// -----------------
	private static final Logger  LoggerInst = new Logger();
	
	public static Logger getInstance(){
		return LoggerInst;
	}	
	
	
	Configuration configuration = Configuration.getInstance();
	BrokerIF broker;
	LinkedList<String> logger;
	LinkedList<TransactionBean> transactionQueue;

	// Logging colors:
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";	
	public static final String ANSI_BOLD  = "\u001b[1m";
	
	private Logger(){
		
		this.logger = new LinkedList<String>();
		transactionQueue = new LinkedList<TransactionBean>();
		System.out.println("__Constructor__ Logger, Ready to recieve Events");
		
	}
	
	public void addEvent(String nextEvent){
		Calendar currTime = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("DD.MM.YYYY,hh.mm");
		String currTimeStr = sdf.format(currTime.getTime());
		
		logger.addLast(nextEvent + " | " + currTimeStr );
	}
	
	public void addTransaction(TransactionBean transactionBean){
		transactionQueue.add(transactionBean);
	}
	
	
	public String print(){
		String loggerString = "";
		
		if (configuration.reportType != ReportType.REPORT_TRANSACTION){
			for(String nextLogEvent : logger){
				loggerString += nextLogEvent + "\n";
				System.out.println(loggerString + "\n");
			}
		}
		
		loggerString += "-----    TRANSACTION SUMMERY    -----\n";
		for (TransactionBean transaction : transactionQueue){
			
			
			loggerString += transaction.print();
		
		
		}
		
		return loggerString;
		
	}
	
	// Print all data, Including current funds:
	// ----------------------------------------
	public String printAll(){
		String loggerString = "";
		
		if (configuration.reportType != ReportType.NO_REPORT){
			loggerString = print();
		}
		
		loggerString += FundsMan.getInstance().getStatus();
				
		return loggerString;
		
	}
	
	// Store all events to logFile, delete events when done:
	// -----------------------------------------------------
	
	public void storeToLog(){
		
		// Create String of all events
		String allEventStr = "";
		for (String nextEvent : logger){
			allEventStr = nextEvent + "\n";
		}
		
		// Append string to end of log file
		FileWriter writer;
		try {
			writer = new FileWriter(configuration.logFile,true);
			writer.write(allEventStr);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		
		// delete current events in logger
		logger.clear();
	}
	
}
