package com.algoTrader.supportCluster;

import com.tictactec.ta.lib.MAType;

public class SRSICfg {

	private static final SRSICfg SRSICfg = new SRSICfg();
	
	// Public static parameters:
	// -------------------------
	public static int 	srsiSimTicksParam = 30;
	public static int 	srsiFastKParam	  = 6;
	public static int 	srsiFastDParam	  = 12;
	public static int 	srsiBuyParam	  = 20;
	public static int 	srsiSellParam	  = 80 ;
	public static MAType 	srsiMATypeDParam  = MAType.Wma;

	
	public static SRSICfg getInstance(){
		return SRSICfg;
	}	
	
        public void print(){
            System.out.println("SRSI configuration:");
            System.out.println("\tsrsiSimTicksParam : " + srsiSimTicksParam);  
            System.out.println("\tsrsiFastKParam    : " + srsiFastKParam   );  
            System.out.println("\tsrsiFastDParam    : " + srsiFastDParam   );  
            System.out.println("\tsrsiBuyParam      : " + srsiBuyParam	   );  
            System.out.println("\tsrsiSellParam     : " + srsiSellParam	   );  
            System.out.println("\tsrsiMATypeDParam  : " + srsiMATypeDParam );  
            System.out.println("-------------------------------");
        }
}
