package com.algoTrader.supportCluster;

public class ScreenerCfg {

	private static final ScreenerCfg screenerCfg = new ScreenerCfg();
	
	public static ScreenerCfg getInstance(){
		return screenerCfg;
	}
	
	public boolean useFilterAtStart = true;
	public long filteringVolume = 1000000;
	public double filteringPrice = 1;
	
}
