package com.algoTrader.supportCluster;

import com.algoTrader.supportCluster.Types.BrokerType;
import com.algoTrader.supportCluster.Types.ConfirmationType;
import com.algoTrader.supportCluster.Types.ProviderType;
import com.algoTrader.supportCluster.Types.ReportType;
import com.algoTrader.supportCluster.Types.TradeType;



public class Configuration {
	
	// Singleton Implementation:
	// -------------------------
	
	private static final Configuration  GeneralCFG = new Configuration();
	
	public static Configuration getInstance(){
		return GeneralCFG;
	}
	
	
	// ----------------------------------- //
	// - General Configuration Variables - //
	// ----------------------------------- //
	// tradeType:
	//		OFFLINE_SIMULATED: 
	//			create offline database with daily tick, iterate on the ticks to simulate real trade
	//			uses simulated broker
	//		ONLINE_SIMULATED:
	//			simulate online trade, request on time price and daily ticks
	//			using online broker
	//		ONLINE:
	//			real trade - not yet implemented
	//
	// confirmationType:
	// 		AUTO_CONFIRMATION:
	//			Auto confirm requests, can use simulated time delay and simulated rejects 
	// 		SHELL_CONFIRMATION:
	//			Prompt human interface shell confirm
	// 		GUI_CONFIRMATION:
	//			Generate GUI with information about the stock and prompt button click confirm
	// 		WEB_CONFIRMATION:
	//			Generate web page  with information about the stock and prompt button click confirm
	//			uses MySql database
	//
	// BrokerType
	//		SIMULATED_BROKER:
	//			Simulate broker functionality, can add random delay to simulate real broker behavior
	//      TWS_SIMULATED_BROKER:
	//			Not in use - simulate TWS broker behavior
	//      TWS_ONLINE_BROKER:
	//			Use TWS online broker
	//      ONLINE_BROKER:
	//			Not in use
	//
	// providerType
	//		YAHOO_PROVIDER:
	//			uses yahoo csv API to for stock information
	//		IB_PROVIDER:
	//			uses IB data base as for stocks information
	//
	//
	// simulatedSetupPhase: for simulated online trade, receive last 'setupTicks' daily ticks for filtered stocks
	// initialFunds: initial trade funds in $
    public TradeType    	tradeType       	= TradeType.ONLINE_SIMULATED;
    public ConfirmationType confirmationType 	= ConfirmationType.AUTO_CONFIRMATION;
    public BrokerType		brokerType			= BrokerType.TWS_ONLINE_BROKER;
    public ProviderType 	providerType 		= ProviderType.IB_PROVIDER;
    public boolean			simulatedSetupPhase = true;
	public Double       	initialFunds    	= 10000.0;
	public String			twsHost				= "127.0.0.1";
	public int				twsPort				= 4001;
	public int				twsID				= 0;
	
	// ------------------------ //
	// - Logger Configuration - //
	// ------------------------ //
	// reportType:
    //		REPORT_SUMMERY:
	//			Only report balance at the end of test
    //		REPORT_TRANSACTION:
	//			Same as REPORT_SUMMERY with additional transaction report 
    //		REPORT_TICKS:
	//			Same as REPORT_TRANSACTION with additional ticks report
    //		REPORT_ALL:
	//			Report all
	// enableColor:
	//		Use colors for some features
	// enableLogFile: 
	//		Store report in log file
	public ReportType	reportType 			= ReportType.REPORT_TRANSACTION;
	public boolean 		enableColor			= true;
	public boolean	    enableLogFile   	= false;
	
	// ----------------------- //
	// - Trade Configuration - //
	// ----------------------- //
    // setupTicks      -  amount of ticks before trade starts
    // minGradeSell    -  minimum grade for stock sale
    // minGradeBuy     -  minimum grade for stock buy
    // minDailySamples -  daily base indicators minimum ticks
	public Integer 	setupTicks			= 10;
	public Double 	minGradeSell 		= 1.0;
	public Double 	minGradeBuy 		= 1.0;
	public Integer	minDailySamples		= 10;
	
	
	// Simulation Configuration:
	// -------------------------
    // Configurations for simulated trade
    // startDate      	- simulated trade start date	
    // endDate        	- simulated trade end date	
	// sample 	  		- delay between each trade cycle
    // simulatedDelay	- simulated response delay
    public String 	startDate 		= "1.6.2013";
	public String 	endDate   		= "1.10.2013";
    public String 	sample 			= "d";
	public Boolean 	simulatedDelay 	= false;
	
	
	// FileIo Configuration:
	// ---------------------
    // allStocks	     	- list of all avaliable stocks
    // filteredStockFile 	- list of filtered stocks to trade with
    // storeDBFile 	     	- location of DB dump
    // logFile	     		- location of log file
	public String allStocks			= "Res/stockList.csv";	
	public String filteredStockFile = "Res/filteredStocks.csv";
	public String storeDBFile 		= "Res/stockDB.DBF";
	public String logFile			= "Res/stockLog.log";
	
	// DataBase configuration:
	// -----------------------
    // maximumNumOfSamples - store this amount of samples for each stock
	public Integer maximumNumOfSamples	= 50;
	
	
	// --------------------- //
	// - Sub Configuration - //
	// --------------------- //
	
	public YAPICFG yapiCfg;
	public MACDCfg macdCfg;
	public SRSICfg srsiCfg;
    public IndicatorsCfg indicatorsCfg;
	public OnlineTimeCfg onlineTimeCfg;
	public ScreenerCfg screenerCfg; 
	
	private Configuration(){
		
		System.out.println("Creating sub Configurations");
		yapiCfg = YAPICFG.getInstance();
		macdCfg = MACDCfg.getInstance();
		srsiCfg = SRSICfg.getInstance();
		indicatorsCfg = IndicatorsCfg.getInstance();
		onlineTimeCfg = OnlineTimeCfg.getInstance();
		screenerCfg = ScreenerCfg.getInstance();
	}

        public void print(){
            System.out.println("-----------------------------------");
            System.out.println("--   ALGO TRADER CONFIGURATION   --");
            System.out.println("-----------------------------------\n\n");
            System.out.println("General configuration:");
            System.out.println("\ttradeType: " + tradeType );
            System.out.println("\tinitialFunds: " + initialFunds);
            if ( tradeType == TradeType.OFFLINE_SIMULATED ){
                System.out.println("\tConfiguration for offline simulated trade:");
                System.out.println("\t\tusing simulatedBroker");
                System.out.println("\t\tstartDate     : " + startDate     );
                System.out.println("\t\tendDate       : " + endDate       );
                System.out.println("\t\tsample        : " + sample 	 	);
                System.out.println("\t\tsimulatedDelay: " + simulatedDelay);
            }
            if ( tradeType == TradeType.ONLINE_SIMULATED ){
                System.out.println("\tConfiguration for online simulated trade:");
                System.out.println("\t\tsimulatedSetupPhase: " +  simulatedSetupPhase);
                System.out.println("\t\tBroker Type: " + brokerType);
                System.out.println("\t\tproviderType: " + providerType);
            }
            System.out.println("\tconfirmationType: " + confirmationType );
            
            System.out.println("\tLogging configuration:");
            System.out.println("\t\treportType: " + reportType);
            System.out.println("\t\tenableColor: " + enableColor);
            System.out.println("\t\tenableLogFile: " + enableLogFile);
            
            System.out.println("-------------------------------");
            System.out.println("Trade configuration:");
            System.out.println("\tsetupTicks      : " + setupTicks     );
            System.out.println("\tminGradeSell    : " + minGradeSell   );
            System.out.println("\tminGradeBuy     : " + minGradeBuy    );
            System.out.println("\tminDailySamples : " + minDailySamples);
            
            System.out.println("-------------------------------");
            System.out.println("Data base configuration:");
            System.out.println("\tmaximumNumOfSamples : " + maximumNumOfSamples);
            System.out.println("-------------------------------");
            
            onlineTimeCfg.print();
            macdCfg.print();        
            srsiCfg.print();
            indicatorsCfg.print();
            System.out.println("----------------------------");
            System.out.println("--   END CONFIGURATION    --");
            System.out.println("----------------------------\n\n");
        }
}
