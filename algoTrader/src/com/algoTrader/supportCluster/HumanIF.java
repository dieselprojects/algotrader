package com.algoTrader.supportCluster;

import java.util.LinkedList;

import com.algoTrader.brokerCluster.Bid;

public interface HumanIF {

	public Bid getConfirmation(Bid requestBid);
	
	public LinkedList<Bid> getConfirmation(LinkedList<Bid> bidsList);
}
