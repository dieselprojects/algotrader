package com.algoTrader.supportCluster;

import java.text.DecimalFormat;

public class FundsMan {
	
	// ---------------------------- //
	// - Singleton implementation - //
	// ---------------------------- //

	private static final FundsMan funds = new FundsMan();
	
	public static FundsMan getInstance(){
		return funds;
	}
	
		
	// ------------------- //
	// - Class variables - //
	// ------------------- //
	
	private Double 	initialFunds  ;
	private Double 	currentFunds  ;
	private Double 	investedFunds ;
	Configuration 	configuration;
	Logger 			logger;
	
	private FundsMan(){
		
		// Configuration:
		configuration = Configuration.getInstance();	
		System.out.println("__Constructor__ FundsMan, initial Funds: "+ configuration.initialFunds);

		
		// Setting initial Funds
		this.initialFunds = configuration.initialFunds;
		this.currentFunds = configuration.initialFunds;
		this.investedFunds = 0.0;
		

		// Logger:
		logger = Logger.getInstance();
		
	}
	
	// ----------------- //
	// - Class Methods - //
	// ----------------- //
	
	// purchase stock, by Amount:
	// --------------------------
	public boolean purchaseStock(String ticker, Double price, int amount){
		if ( price*amount > getCurrentFunds() ){
			
			String errorTransaction = new String("__ERROR__ not enough funds to buy this amount of Stocks, Ticker: "+ ticker +  ", Price: " + price +", Amount: " + amount);
			
			System.out.println(errorTransaction);
			logger.addEvent(errorTransaction);
			
			return false;
		
		} else {
			
			String lastTransaction = new String("__BUY__ Ticker: " + ticker + ", Price: " + price + ", Amount: " + amount);
			
			System.out.println(lastTransaction);
			setCurrentFunds(getCurrentFunds() - price*amount);
			investedFunds += price*amount;
			logger.addEvent(lastTransaction);
			
			return true;
		
		}		
	}
	
	// Sell stocks, By amount:
	// -----------------------
	public boolean sellStock(String ticker, Double price, int amount, Double buyPrice){
		
		if (buyPrice*amount > investedFunds){
			
			String errorTransaction = new String("__ERROR__ sell more than invested funds, Ticker: "+ ticker +  ", BuyPrice: " + buyPrice +", Amount: " + amount);
			System.out.println(errorTransaction);
			
			logger.addEvent(errorTransaction);
			
			return false;
		
		} else {
			
			String lastTransaction;
			
			setCurrentFunds(getCurrentFunds() + price*amount);
			investedFunds -= buyPrice*amount;
			
			if (price > buyPrice){
				
				lastTransaction = new String("__Sell Profit__ Ticker: " + ticker + ", Price: " + price + ", BuyPrice: " + buyPrice + ", Amount: " + amount + "Total Profit: " + (price - buyPrice)*amount);
							
			} else {
				
				lastTransaction = new String("__Sell Loss__ Ticker: " + ticker + ", Price: " + price + ", BuyPrice: " + buyPrice + ", Amount: " + amount + "Total Profit: " + (price - buyPrice)*amount);
			
			}
			
			System.out.println(lastTransaction);
			logger.addEvent(lastTransaction);
			
			return true;
		
		}		
	}
	
	
	// Purchase Stocks, By Funds ( will buy as many Stocks )
	// -----------------------------------------------------
	public int purchaseStock(String ticker, Double price, float funds){
		if ( price > getCurrentFunds() ){
			
			String errorTransaction = new String("__ERROR__ not enough funds to buy minimus amount of Stocks, Ticker: "+ ticker +  ", Price: " + price + ",Current Funds: " + getCurrentFunds());
			
			System.out.println(errorTransaction);
			logger.addEvent(errorTransaction);
			
			return 0;
		
		} else {
			
			int amount = (int) (funds/price);
			
			String lastTransaction = new String("__BUY__ Ticker: " + ticker + ", Price: " + price + ", Amount: " + amount);
			
			System.out.println(lastTransaction);
			setCurrentFunds(getCurrentFunds() - price*amount);
			investedFunds += price*amount;
			logger.addEvent(lastTransaction);
			
			return amount;
		
		}		
	}
	
	// -------------------- //
	// - GetCurrent funds - //
	// -------------------- //

	public Double getCurrentFunds() {
		return currentFunds;
	}


	// -------------------- //
	// - SetCurrent funds - //
	// -------------------- //
	
	public void setCurrentFunds(Double currentFunds) {
		this.currentFunds = currentFunds;
	}

	// --------------------- //
	// - Get Initial Funds - //
	// --------------------- //
	
	public Double getInitialFunds() {
		return initialFunds;
	}

	// --------------------- //
	// - Set Initial Funds - //
	// --------------------- //
	
	public void setInitialFunds(Double initialFunds) {
		this.initialFunds = initialFunds;
	}
	
	
	// ------------------- //
	// - Get Total Funds - //
	// ------------------- //
	
	public Double getTotalFunds() {
		return currentFunds + investedFunds;
	}
	
	
	// -------------- //
	// - Get Profit - //
	// -------------- //
	
	public Double getProfit() {
		return ((currentFunds + investedFunds)/initialFunds - 1.0)*100.0;
	}
	
	// ------------- //
	// - GetStatus - //
	// ------------- //
	
	// Returns String with Current Funds, InvestedFunds, and InitialFunds
	
	public String getStatus(){
		String 	statusString = "\nFunds Manager, Report:\n";
				statusString += "----------------------\n";
		String profitString = "";
		
		statusString += "Inital funds: " + initialFunds + "\n";
		statusString += "Current funds: " + currentFunds + "\n";
		statusString += "Invested funds: " + investedFunds + "\n";
		statusString += "Total funds: " + getTotalFunds() + "\n";
		
		profitString = ( getProfit() > 0 ) ? new DecimalFormat("##.##").format(getProfit()) : "(" + new DecimalFormat("##.##").format(getProfit()) + ")";
		statusString += "Profit: " + profitString + "%\n";
		
		return statusString;
	}
	
	
	

}
