package com.algoTrader.supportCluster;

public class Types{

	// ----------------------------------- //
	// -- Configuration related structs -- //
	// ----------------------------------- //
    public enum TradeType {
        OFFLINE_SIMULATED,
        ONLINE_SIMULATED,
        ONLINE
    }
    
    public enum ConfirmationType {
    	AUTO_CONFIRMATION,
    	SHELL_CONFIRMATION,
    	GUI_CONFIRMATION,
    	WEB_CONFIRMATION
    }
    
    public enum BrokerType {
    	SIMULATED_BROKER,
    	TWS_SIMULATED_BROKER,
    	TWS_ONLINE_BROKER,
    	ONLINE_BROKER		
    }
    
    public enum ProviderType {
    	YAHOO_PROVIDER,
    	IB_PROVIDER
    }

    
    public enum ReportType {
    	NO_REPORT,
    	REPORT_TRANSACTION,
    	REPORT_TICKS,
    	REPORT_ALL
    }
    
    // -------------------------------- //
    // -- Controller related structs -- //
    // -------------------------------- //
    
    // Transaction operation type
    public enum OPType {
    	BUY,
    	SELL
    }
    
    
    
}
