package com.algoTrader.supportCluster;

// Encapsulate time instance
public class TimeInst {
	long 	timeInMs;
	boolean dailyTick;
	
	TimeInst(){
		this.timeInMs 	= 0;
		this.dailyTick 	= false;
	}

	TimeInst(boolean dailyTick,
			long timeInMs
			){
		this.timeInMs 	= timeInMs ;
		this.dailyTick 	= dailyTick;
	}
}
