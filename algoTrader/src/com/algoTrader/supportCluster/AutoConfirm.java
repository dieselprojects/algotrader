package com.algoTrader.supportCluster;

import java.util.LinkedList;

import com.algoTrader.brokerCluster.Bid;

public class AutoConfirm implements HumanIF {

	@Override
	public Bid getConfirmation(Bid requestBid) {
		System.out.println("Inside auto confirm");
		requestBid.approvedHuman = true;
		return requestBid;
	}

	@Override
	public LinkedList<Bid> getConfirmation(LinkedList<Bid> bidsList) {
		LinkedList<Bid> approvedBids = new LinkedList<Bid>();
				
		for(Bid nextBid : bidsList){
			Bid returnedBid = getConfirmation(nextBid);
		
			if (returnedBid.approvedHuman == true){
				approvedBids.addLast(returnedBid);
			}
		}
	
		return approvedBids;		
		
	}

}
