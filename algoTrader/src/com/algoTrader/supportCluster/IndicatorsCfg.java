package com.algoTrader.supportCluster;

public class IndicatorsCfg {

	private static final IndicatorsCfg IndicatorsCfg = new IndicatorsCfg();
	
        // Indicators grade factors ( 0 - disable )
        //CDL3INSIDE          Three Inside Up // Weak
	//CDL3OUTSIDE         Three Outside Up // GOOD
	//CDL3STARSINSOUTH    Three Stars In The South // Strongest
	//CDL3WHITESOLDIERS   Three Advancing White Soldiers// Strong 
	//CDLABANDONEDBABY    Abandoned Baby // Strong	
        public static int cdl3InsideGradeFactor        = 1;
	public static int cld3outsideGradeFactor       = 10;
	public static int cld3StarsInSouthGradeFactor  = 50;
	public static int cld3WhiteSolGradeFactor      = 20;

	public static IndicatorsCfg getInstance(){
		return IndicatorsCfg;
	}

        public void print(){
            System.out.println("Indicators configuration:");
            System.out.println("\tcdl3InsideGradeFactor       : " + cdl3InsideGradeFactor       );  
            System.out.println("\tcld3outsideGradeFactor      : " + cld3outsideGradeFactor      );  
            System.out.println("\tcld3StarsInSouthGradeFactor : " + cld3StarsInSouthGradeFactor );  
            System.out.println("\tcld3WhiteSolGradeFactor     : " + cld3WhiteSolGradeFactor     );
            System.out.println("-------------------------------");
        }
}
