package com.algoTrader.supportCluster;


// DEPRICATED
public class YAPICFG {
	
	private static final long SECOND = 1000;
	private static final long MINUTE = SECOND*60;
	private static final long HOUR = MINUTE*60;
	private static final long DAY = HOUR*24;
	
	
	private static final YAPICFG  YAPICFGO = new YAPICFG();

	private static long nextUpdateTime              = 3*HOUR;
	private static long     nextDBUpdateTime        = DAY;
	private static String   fileDB;
	private static String 	fileAllStocks;
	private static String 	fileFilteredStocks;
	private static String 	stockInputFile;
	private static boolean 	useFiltered             = false;
	private static boolean  firstUse                = false;
	private static int	volumeFilter            = 3000000; 
	private static int     	openTradeHour           = 0 ;
	private static int     	closeTradeHour          = 16;
	private static int	updateStocksIteration   = 3; // In hours
	
	public static YAPICFG getInstance(){
		return YAPICFGO;
	}
	
	// Update Time:
	// ------------
	public long getNextUpdateTime(){
		return nextUpdateTime;
	}
	
	public void setNextUpdateTime(long nextUpdateTime){
		YAPICFG.nextUpdateTime = nextUpdateTime;
	}
	
	// DataBase Update Time:
	// ---------------------
	public long getNextDBUpdateTime(){
		return nextDBUpdateTime;
	}
	
	public void setNextDBUpdateTime(long nextDBUpdateTime){
		YAPICFG.nextDBUpdateTime = nextDBUpdateTime;
	}
	
	// file DataBase:
	// --------------
	public String getFileDB(){
		return YAPICFG.fileDB;
	}
	
	public void setFileDB(String fileDB){
		YAPICFG.fileDB = fileDB;
	}
	
	// file All Stocks:
	// ----------------
	public String getFileAllStocks(){
		return fileAllStocks;
	}
	
	public void setFileAllStocks(String fileAllStocks){
		YAPICFG.fileAllStocks = fileAllStocks;
	}
	
	// file Filtered Stocks:
	// ---------------------
	public String getFileFilteredStocks(){
		return fileFilteredStocks;
	}
	
	public void setFileFilteredStocks(String fileFilteredStocks){
		YAPICFG.fileFilteredStocks = fileFilteredStocks;
	}
	
	// Main stocks input file:
	// -----------------------
	public String getStockInputFile(){
		return stockInputFile;
	}
	
	public void setStockInputFile(String stockInputFile){
		YAPICFG.stockInputFile = stockInputFile;
	}

	// use filtered file:
	// ------------------
	public boolean getUseFilter(){
		return useFiltered;
	}
	
	public void setUseFiltered(boolean useFiltered){
		YAPICFG.useFiltered = useFiltered;
	}
	
	// first Use:
	// ----------
	public boolean getFirstUse(){
		return firstUse;
	}
	
	public void setFirstUse(boolean firstUse){
		YAPICFG.firstUse = firstUse;
	}
	
	// Volume filter:
	// --------------
	public int getVolumeFilter(){
		return volumeFilter;
	}
	
	public void setVolumeFilter(int volumeFilter){
		YAPICFG.volumeFilter = volumeFilter;
	}
	
	// Open Trade Hour :
	// -----------------
	public int getOpenTradeHour(){
	return openTradeHour;
	}
		
	public void setOpenTradeHour(int openTradeHour){
		YAPICFG.openTradeHour = openTradeHour;
	}
	
	// Close Trade Hour :
	// -----------------
	public int getCloseTradeHour(){
	return closeTradeHour;
	}
		
	public void setCloseTradeHour(int closeTradeHour){
		YAPICFG.closeTradeHour = closeTradeHour;
	}
	
	// Update stocks iteration during trade in hours:
	// ----------------------------------------------
	public int getUpdateStocksIteration(){
	return updateStocksIteration;
	}
		
	public void setUpdateStocksIteration(int updateStocksIteration){
		YAPICFG.updateStocksIteration = updateStocksIteration;
	}
        
        
        public void print(){
            System.out.println("\n");
            System.out.println("YAPI configuration:");
            System.out.println("-------------------");
        
        }
}
