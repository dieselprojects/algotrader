package com.algoTrader.supportCluster;

import com.tictactec.ta.lib.MAType;

public class MACDCfg {

	private static final MACDCfg MACDCfg = new MACDCfg();
	
	// Public static parameters:
	// -------------------------
	public static int 	macdSimTicksParam	= 30;
	public static int 	macdFastParam		= 8;
	public static int 	macdSlowParam		= 20;
	public static int 	macdSignalParam		= 9;
	public static int 	macdTickBeforeParam	= 3 ;
	public static double 	macdIndicaitonMargain 	= 0.1;
	public static MAType 	macdMATypeFastParam 	= MAType.Ema;
	public static MAType 	macdMATypeSlowParam 	= MAType.Ema;
	public static MAType 	macdMATypeSignalParam 	= MAType.Ema;
	
	public static MACDCfg getInstance(){
		return MACDCfg;
	}	

        public void print(){
            System.out.println("MACD configuration:");
            System.out.println("\tmacdSimTicksParam     : " + macdSimTicksParam    );  
            System.out.println("\tmacdFastParam         : " + macdFastParam        );  
            System.out.println("\tmacdSlowParam         : " + macdSlowParam        );  
            System.out.println("\tmacdSignalParam       : " + macdSignalParam      );  
            System.out.println("\tmacdTickBeforeParam   : " + macdTickBeforeParam  );  
            System.out.println("\tmacdIndicaitonMargain : " + macdIndicaitonMargain);  
            System.out.println("\tmacdMATypeFastParam   : " + macdMATypeFastParam  );  
            System.out.println("\tmacdMATypeSlowParam   : " + macdMATypeSlowParam  );  
            System.out.println("\tmacdMATypeSignalParam : " + macdMATypeSignalParam);
            System.out.println("-------------------------------");
        }
}
