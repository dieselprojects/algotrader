package com.algoTrader.main;

import com.algoTrader.controllerCluster.OnlineAlgoTrade;
import com.algoTrader.controllerCluster.SimulatedAlgoTrade;
import com.algoTrader.supportCluster.Configuration;
import com.algoTrader.supportCluster.Screener;
import com.algoTrader.supportCluster.Types.TradeType;

public class algoTraderMain {

	public static void main(String[] args) {
		
	    Configuration configuration;
		configuration = Configuration.getInstance();
		Screener screener = new Screener();
		
		System.out.println("----------------------");
		System.out.println("-- Start AlgoTrader --");
		System.out.println("----------------------\n");
        configuration.print();
        
        if (configuration.screenerCfg.useFilterAtStart == true){
            screener.screenStocks();
        }
	
        switch (configuration.tradeType){
        case OFFLINE_SIMULATED:
			System.out.println("Creating simutaltedAlgoTrade");
			SimulatedAlgoTrade simulatedTest = new SimulatedAlgoTrade();
			
			System.out.println("Calling simulatedTest.SimulationStarter()");
			simulatedTest.SimulationStarter();
        	break;
        case ONLINE_SIMULATED:
			System.out.println("Creating OnlineAlgoTrade");
			OnlineAlgoTrade onlineTest = new OnlineAlgoTrade();
			
			System.out.println("Calling onlineTest.SimulationStarter()");
			onlineTest.AlgoStarter(true);
        	break;
        case ONLINE:
        	System.err.println("TODO: add support for real online implementation");
        	System.err.println("Exiting...");
        	System.exit(1);
        	break;
        default:
        	System.err.println("Not valid tradeType selection");
        	System.err.println("Exiting...");
        	System.exit(1);
        	break;
        }
                
		System.out.println("\n----------------------");
		System.out.println("-- Ended AlgoTrader --");
		System.out.println("----------------------\n\n");
		
		System.exit(0);
	}

}
